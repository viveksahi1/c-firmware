/**
 ******************************************************************************
 * @file    myoslib/s4380479_os_imu_log.c
 * @author  Vivek Sahi - 43804791
 * @date    11-03-2019
 * @brief   OS imu log drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_imu_log.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define IMU_LOG_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 4)
#define IMU_LOG_PRIORITY (tskIDLE_PRIORITY + 6)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char dataToPrint[IMU_DATA_BUFFER];
char *dataPtr;

/**
 * Initialises the pins used for the system monitor
 */
void s4380479_TaskImuLog(void)
{
    struct ImuMessage imuMsg;
    int fileAcc = 0, fileMag = 0;
    signed short fdAcc;
    signed short fdMag;

    while (1)
    {
        if (s4380479_QueueImuLog != NULL)
        {

            if (xQueueReceive(s4380479_QueueImuLog, &imuMsg, __UINT32_MAX__))
            {
                switch (imuMsg.type)
                {
                    // Handle ACC
                    case OPEN_ACC_FILE:

                        if (!fileAcc)
                        { // file not already opened
                            fdAcc = s4380479_open_log_file(imuMsg.data);
                            s4380479_clean_file(fdAcc);
                            fileAcc = 1;
                        } else {
                            s4380479_os_log_message(ERROR_MSG, "CANNOT OPEN ANOTHER ACC FILE");
                        }

                        break;
                    case ACC_DATA:
                        if (fileAcc)
                        {
                            s4380479_write_log_file(fdAcc, imuMsg.data);
                        } else {
                            s4380479_os_log_message(ERROR_MSG, "CANNOT WRITE ACC FILE");
                        }
                        break;
                    case CLOSE_ACC_FILE:
                        if (fileAcc)
                        {
                            s4380479_close_log_file(fdAcc);
                            fileAcc = 0;
                        } else {
                            s4380479_os_log_message(ERROR_MSG, "CANNOT CLOSE ACC FILE");
                        }
                        break;
                    // Handle MAG
                    case OPEN_MAG_FILE:

                        if (!fileMag)
                        { // file not already opened
                            fdMag = s4380479_open_log_file(imuMsg.data);
                            s4380479_clean_file(fdMag);
                            fileMag = 1;
                        } else {
                            s4380479_os_log_message(ERROR_MSG, "CANNOT OPEN ANOTHER MAGO FILE");
                        }

                        break;
                    case MAG_DATA:
                        if (fileMag)
                        {
                            s4380479_write_log_file(fdMag, imuMsg.data);
                        } else {
                            s4380479_os_log_message(ERROR_MSG, "CANNOT WRITE TO MAGO FILE");
                        }
                        break;
                    case CLOSE_MAG_FILE:
                        if (fileMag)
                        {
                            s4380479_close_log_file(fdMag);
                            fileMag = 0;
                        } else {
                            s4380479_os_log_message(ERROR_MSG, "CANNOT CLOSE MAGO FILE");
                        }
                        break;
                    default:
                        break;
                }
                vTaskDelay(10);
            }
        }
    }
}

/** task safe printf function */
void s4380479_os_imu_log(int messageType, char *message, ...)
{
    struct ImuMessage imuMsg;

    imuMsg.type = messageType;
    //receive arguments
    //receive arguments
    va_list args;
    va_start(args, message);

    dataPtr = imuMsg.data;
    vsprintf(dataPtr, message, args);
    va_end(args);

    if (xQueueSend(s4380479_QueueImuLog, (void *)&imuMsg,
                   (portTickType)100) != pdPASS)
    {
        //failed
        printf("%s\r\n", "failied to send queue");
    }
}

/**
 * Initialises the task for printing messages to stdout
 */
void s4380479_os_imu_log_init(void)
{
    //init Queue
    s4380479_QueueImuLog = xQueueCreate(5, sizeof(char) * IMU_DATA_BUFFER);

    if (xTaskCreate((TaskFunction_t)&s4380479_TaskImuLog, "IMULOG", IMU_LOG_TASK_STACK_SIZE, NULL,
                    IMU_LOG_PRIORITY, NULL) != pdPASS)
    {
        printf("%s\r\n", "failed to create debug printf task");
    }
}
