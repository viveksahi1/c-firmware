/**
 ******************************************************************************
 * @file    mylib/s4380479_os_pantilt.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   pantilt os drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskPanTilt(void) - Task to control the pan and tilt,
 * 											and laser
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include "s4380479_os_pantilt.h"
#include <s4380479_os_joystick.h>
#include "s4380479_hal_pantilt.h"
#include "s4380479_hal_sysmon.h"
#include "FreeRTOS.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
#define PT_TASK_STACK_SIZE		(configMINIMAL_STACK_SIZE * 2)
#define PT_PRIORITY 			(tskIDLE_PRIORITY + 2)

TaskHandle_t s4380479_PT_Handle;

int pan = 0;
int tilt = 0;

/**
 * Initialises the pins used for the system monitor
 */
void s4380479_TaskPanTilt(void) {

	struct PanMessage msgPan;
	struct TiltMessage msgTilt;

	while (1) {

		/* check queues */
		if (s4387479_QueuePan != NULL) {
			if (xQueueReceive(s4387479_QueuePan, &msgPan, 1)) {

				pan = ((msgPan.angle * - 21) / 100) + 35;

			}
		}

		if (s4387479_QueueTilt != NULL) {

			if (xQueueReceive(s4387479_QueueTilt, &msgTilt, 1)) {

				tilt = (msgTilt.angle  * 15 / 200) - 70;
			}
		}
		/* fix angle boundaries */
		if (pan < -70) {

			pan = -70;
		} else if (pan > 70) {

			pan = 70;
		}
		if (tilt < -70) {

			tilt = -70;
		} else if (tilt > 70) {

			tilt = 70;
		}

		s4380479_hal_pantilt_pan_write(pan);
		s4380479_hal_pantilt_tilt_write(tilt);

		vTaskDelay(10);
	}

}

/**
 * Init semaphoreas and  queues and creatse a new pan tilt task
 */
extern void s4380479_os_pantilt_init() {

	s4380479_hal_pantilt_init();

	/* create queues and semaphores*/
	s4387479_SemaphorePanLeft = xSemaphoreCreateBinary();
	s4387479_SemaphorePanRight = xSemaphoreCreateBinary();
	s4387479_SemaphoreTiltUp = xSemaphoreCreateBinary();
	s4387479_SemaphoreTiltDown = xSemaphoreCreateBinary();
	s4387479_QueuePan = xQueueCreate(10, sizeof(struct PanMessage));
	s4387479_QueueTilt = xQueueCreate(10, sizeof(struct TiltMessage));

	xTaskCreate(&s4380479_TaskPanTilt, "PT", PT_TASK_STACK_SIZE, NULL,
	PT_PRIORITY, s4380479_PT_Handle);
}

