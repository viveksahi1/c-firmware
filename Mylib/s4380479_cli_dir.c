/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_dir.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli dir
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_dir.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableDir; //variable to store dir cli cmds

/**
 * This method allcoates the memory for the argtable to store dir cmds.
 */
void allocate_dir_argtable()
{
    argtableDir = malloc(sizeof(struct arg_str *) * 5 + sizeof(struct arg_lit *) + sizeof(struct arg_end *) + sizeof(void *) + sizeof(char) * 10);
}

/**
 * Initialise the dir cmds
 */
void s4380479_init_dir_cmd()
{
    /** dir CLI CMD */
    // struct arg_rex *logCmd = arg_rex1(NULL, NULL, "dir", NULL, REG_ICASE, NULL);
    struct arg_str *createDir = arg_str1("c", "create", "dirname", "create new dir with name specified");
    struct arg_str *listDir = arg_str0("l", "list", NULL, "list files in current directory");
    struct arg_str *treeDir = arg_str0("t", "tree", NULL, "Directory tree");
    struct arg_str *deleteDir = arg_str1("d", "delete", "dirname", "delete a directory with name specified");
    struct arg_lit *helpDir = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *endDir = arg_end(20);

    //redirect error func
    createDir->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    listDir->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    treeDir->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    deleteDir->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    helpDir->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    endDir->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_dir_argtable();
    argtableDir[dCreateDir] = createDir;
    argtableDir[dListDir] = listDir;
    argtableDir[dDeleteDir] = deleteDir;
    argtableDir[dHelpDir] = helpDir;
    argtableDir[dEndDir] = endDir;
    argtableDir[dTreeDir] = treeDir;
}

/**
 * This method return the artable for the dir cmd
 */
void **s4380479_get_dir_cmd()
{
    return argtableDir;
}

/**
 * This is the callback function to hadndle dir cmds
 */
void handle_dir(int dirCreate, char *createName,
                int dirLs, int treeLs,
                int dirDel, char *delName)
{
    if (dirCreate)
    {
        s4380479_create_dir(createName);
    }
    else if (dirLs)
    {
        s4380479_list_file();
    }
    else if (dirDel)
    {
        s4380479_remove_dir(delName);
    }
    else if (treeLs)
    {
        s4380479_tree();
    }
}
