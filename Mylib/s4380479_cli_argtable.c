/**
 ******************************************************************************
 * @file    myoslib/s4380479_cli_argtable.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli argtable
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_argtable.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/

/**
 * This method prints the allocation error message for the artable cmds
 * Returns 1
 */
int allocation_error(const char *prog)
{
    s4380479_os_log_message(ERROR_MSG, "%s: insufficient memory", prog);
    return 1;
}

/**
 * This method prints the usage for the CLI cmds
 * Returns 1
 */
int print_help(const char *prog, void **argtable)
{

    s4380479_os_log_message(LOG_MSG, "Usage: %s", prog);
    vTaskDelay(10);
    s4380479_os_printf(ANSI_COLOR_GREEN);
    arg_print_syntax(stdout, argtable, "\r");
    arg_print_glossary(stdout, argtable, "  %-20s %s\r\n");
    s4380479_os_printf(ANSI_COLOR_RESET);

    return 1;
}

/**
 * This method handles the error messages for invalid CLI cmds
 * Returns 1
 */
int print_errors(const char *prog, struct arg_end *end)
{
    /* Display the error details contained in the arg_end struct.*/
    arg_print_errors(stdout, end, prog);
    s4380479_os_log_message(LOG_MSG, "Try '%s --help' for more information.\r", prog);
    vTaskDelay(20); //delay to allow stadout flush
    return 1;
}

/**
 * This methos is used to identify the cmds that was entered in the CLI interface
 * Returns the cmds entered if correct, else return unknown cmd.
 */
int identify_cmd_entered(char *cmd)
{
    //compare input to available cmds
    if (!strncmp(cmd, progFile, sizeof(progFile)))
    {
        return FILE_CMD;
    }
    else if (!strncmp(cmd, progDir, sizeof(progDir)))
    {
        return DIR_CMD;
    }
    else if (!strncmp(cmd, progCd, sizeof(progCd)))
    {
        return CD_CMD;
    }
    else if (!strncmp(cmd, progRec, sizeof(progRec)))
    {
        return REC_CMD;
    }

    return UNK_CMD;
}

/**
 * This method is used to redirect all the CLI argument errors.
 */
void cliErrorFunc(void *parent, FILE *fp, int error, const char *argval, const char *progname)
{
    progname = progname ? progname : "";
    argval = argval ? argval : "";

    s4380479_os_log_message(ERROR_MSG, "%s: ", progname);
    switch (error)
    {
    case ARG_ELIMIT:
        s4380479_os_log_message(ERROR_MSG, "%s: too many errors to display", progname);
        break;
    case ARG_EMALLOC:
        s4380479_os_log_message(ERROR_MSG, "%s: insufficent memory", progname);
        break;
    case ARG_ENOMATCH:
        s4380479_os_log_message(ERROR_MSG, "%s: unexpected argument \"%s\"", progname, argval);
        break;
    case ARG_EMISSARG:
        s4380479_os_log_message(ERROR_MSG, "%s: option \"%s\" requires an argument", progname, argval);
        break;
    case ARG_ELONGOPT:
        s4380479_os_log_message(ERROR_MSG, "%s: invalid option \"%s\"", progname, argval);
        break;
    default:
        s4380479_os_log_message(ERROR_MSG, "%s: invalid option ", progname);
        break;
    }
    fputc('\n', fp);
}

/**
 * This method processes all the CLI registered cmds and make callback to functions.
 * Returns 
 *  0 on success and error code on error
 */
int process_cli_cmds(int argc, char **argv)
{
    int cmdCode;

    /** File CLI CMD */
    void **argtableFileCmd = s4380479_get_file_cmd();
    struct arg_hdr **argtableFile = (struct arg_hdr **)argtableFileCmd;

    void **argtableDirCmd = s4380479_get_dir_cmd();
    struct arg_hdr **argtableDir = (struct arg_hdr **)argtableDirCmd;

    void **argtableCdCmd = s4380479_get_cd_cmd();
    struct arg_hdr **argtableCd = (struct arg_hdr **)argtableCdCmd;

    void **argtableRecCmd = s4380479_get_rec_cmd();
    struct arg_hdr **argtableRec = (struct arg_hdr **)argtableRecCmd;

    /* verify the argtable[] entries were allocated sucessfully */
    if (arg_nullcheck(argtableFileCmd) != 0)
    {
        return allocation_error(progFile);
    }
    else if (arg_nullcheck(argtableDirCmd) != 0)
    {
        return allocation_error(progDir);
    }
    else if (arg_nullcheck(argtableCdCmd) != 0)
    {
        return allocation_error(progCd);
    }
    else if (arg_nullcheck(argtableRecCmd) != 0)
    {
        return allocation_error(progRec);
    }
    /* Parse the command line as defined by argtable[] */
    arg_parse(argc, argv, argtableFileCmd);
    arg_parse(argc, argv, argtableDirCmd);
    arg_parse(argc, argv, argtableCdCmd);
    arg_parse(argc, argv, argtableRecCmd);

    cmdCode = identify_cmd_entered(argv[0]);
    // s4380479_os_log_message(ERROR_MSG, "%d|%s|%s|%s|%s", argc, argv[0], argv[1], argv[2], argv[3]);

    /* special case: '--help' takes precedence over error reporting */
    if (((struct arg_lit *)(argtableFile[fHelpFile]))->count > 0 && cmdCode == FILE_CMD)
    {
        return print_help(progFile, argtableFileCmd);
    }
    else if (((struct arg_lit *)(argtableDir[dHelpDir]))->count > 0 && cmdCode == DIR_CMD)
    {
        return print_help(progDir, argtableDirCmd);
    }
    else if (((struct arg_lit *)(argtableCd[cHelpCd]))->count > 0 && cmdCode == CD_CMD)
    {
        return print_help(progCd, argtableCdCmd);
    }
    else if (((struct arg_lit *)(argtableRec[recHelp]))->count > 0 && cmdCode == REC_CMD)
    {
        return print_help(progRec, argtableRecCmd);
    }

    else if (cmdCode == FILE_CMD && argc > 3 && argc < 6)
    {
        // s4380479_os_log_message(ERROR_MSG, "test:%s|%s", ((struct arg_str *)(argtableFile[fMoveFile]))->sval[0],
        //                         ((struct arg_str *)(argtableFile[fMoveFile]))->sval[1]);
        handle_file(((struct arg_str *)(argtableFile[fCreateFile]))->count,
                    ((struct arg_str *)(argtableFile[fCreateFile]))->sval[0],
                    ((struct arg_str *)(argtableFile[fViewFile]))->count,
                    ((struct arg_str *)(argtableFile[fViewFile]))->sval[0],
                    ((struct arg_str *)(argtableFile[fDeleteFile]))->count,
                    ((struct arg_str *)(argtableFile[fDeleteFile]))->sval[0],
                    ((struct arg_str *)(argtableFile[fMoveFile]))->count,
                    ((struct arg_str *)(argtableFile[fMoveFile]))->sval[0],
                    ((struct arg_str *)(argtableFile[fMoveFile]))->sval[1]);
    }
    else if (cmdCode == DIR_CMD && argc >= 3 && argc < 5)
    {
        handle_dir(((struct arg_str *)(argtableDir[dCreateDir]))->count,
                    ((struct arg_str *)(argtableDir[dCreateDir]))->sval[0],
                    ((struct arg_str *)(argtableDir[dListDir]))->count,
                    ((struct arg_str *)(argtableDir[dTreeDir]))->count,
                    ((struct arg_str *)(argtableDir[dDeleteDir]))->count,
                    ((struct arg_str *)(argtableDir[dDeleteDir]))->sval[0]
                    );
    }
    else if (cmdCode == CD_CMD && argc >= 3 && argc < 5)
    {
        handle_cd(((struct arg_str *)(argtableCd[cCd]))->count,
                    ((struct arg_str *)(argtableCd[cCd]))->sval[0]
                    );
    }
    else if (cmdCode == REC_CMD && argc >= 5 && argc <= 7)
    {
        handle_rec(((struct arg_str *)(argtableRec[recEnable]))->count,
                    ((struct arg_str *)(argtableRec[recEnable]))->sval[0],
                    ((struct arg_str *)(argtableRec[recEnable]))->sval[1],
                    ((struct arg_str *)(argtableRec[recEnable]))->sval[2],
                    ((struct arg_str *)(argtableRec[recDisable]))->count,
                    ((struct arg_str *)(argtableRec[recDisable]))->sval[0],
                    ((struct arg_str *)(argtableRec[recDisable]))->sval[1]
                    );
    }
    else
    {
        /* If the parser returned any errors then display them and exit */
        if (cmdCode == FILE_CMD)
        {

            print_errors(progFile, ((struct arg_end *)argtableFile[fEndFile]));
        }
        else if (cmdCode == DIR_CMD)
        {

            print_errors(progDir, ((struct arg_end *)(argtableDir[dEndDir])));
        }
        else if (cmdCode == CD_CMD)
        {

            print_errors(progCd, ((struct arg_end *)(argtableCd[cEndCd])));
        }
         else if (cmdCode == REC_CMD)
        {

            print_errors(progRec, ((struct arg_end *)(argtableRec[recEnd])));
        }
        else
        {
            s4380479_os_log_message(ERROR_MSG, "Try '%s --help' for more information.", progFile);
            vTaskDelay(10); //delay to allow printfs to finihs
        }
    }

    return 0;
}
