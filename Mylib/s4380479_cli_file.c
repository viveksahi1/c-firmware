/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_file.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli file
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_file.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableFile; //variable to store file cli cmds

/**
 * This method allcoates the memory for the argtable to store file cmds.
 */
void allocate_file_argtable()
{
    argtableFile = malloc(sizeof(struct arg_str *) * 4 + sizeof(struct arg_lit *) + sizeof(struct arg_end *) + sizeof(void *) + sizeof(char) * 10);
}

/**
 * Initialise the file cmds
 */
void s4380479_init_file_cmd()
{
    /** file CLI CMD */
    // struct arg_rex *logCmd = arg_rex1(NULL, NULL, "file", NULL, REG_ICASE, NULL);
    struct arg_str *createFile = arg_str1("c", "create", "filename", "create new file with name specified");
    struct arg_str *viewFile = arg_str1("v", "view", "filename",
                                        "view contents of file with name specified");
    struct arg_str *deleteFile = arg_str1("d", "delete", "filename", "delete specified file");
    struct arg_str *moveFile = arg_strn("m", "move", "{filename, directory}", 2, 2,
                                        "move specified file to a directory (not sub directories)");
    struct arg_lit *helpFile = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *endFile = arg_end(20);

    //redirect error func
    createFile->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    viewFile->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    deleteFile->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    moveFile->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    helpFile->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    endFile->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_file_argtable();
    argtableFile[fCreateFile] = createFile;
    argtableFile[fViewFile] = viewFile;
    argtableFile[fDeleteFile] = deleteFile;
    argtableFile[fMoveFile] = moveFile;
    argtableFile[fHelpFile] = helpFile;
    argtableFile[fEndFile] = endFile;
}

/**
 * This method return the artable for the file cmd
 */
void **s4380479_get_file_cmd()
{
    return argtableFile;
}

/**
 * This is the callback function to hadndle file cmds
 */
void handle_file(int fileCreate, char *createName,
                 int fileView, char *viewName,
                 int fileDel, char *delName,
                 int fileMove, char *moveFile, char *moveDir)
{
    if (fileCreate)
    {
        s4380479_create_file(createName);
        s4380479_list_file();
    }
    else if (fileView)
    {
        s4380479_read_file(viewName);
    }
    else if (fileDel)
    {
        s4380479_remove_file(delName);
    }
    else if (fileMove)
    {
        s4380479_tree();
        s4380479_move_file(moveFile, moveDir);
        s4380479_tree();
    }
}
