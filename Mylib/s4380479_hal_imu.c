/**
 ******************************************************************************
* @file    myoslib/s4380479_hal_imu.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal imu drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_imu.h"
// #include "stm32l4_b_l475e_iot01a.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
int16_t acclDataXYZ[3] = {0};
int16_t gyrDataXYZ[3] = {0};
int16_t magnDataXYZ[3] = {0};
/* External function prototypes -----------------------------------------------*/

void s4380479_init_gyr_acc(void)
{
  // init acc and gyro
  BSP_ACCELERO_Init();
  BSP_GYRO_Init();
  BSP_MAGNETO_Init();
}

/**
 * Get acc and gyr readings
 */
void s4380479_read_acc_mag(int device)
{
  if (device)
  {
    BSP_ACCELERO_AccGetXYZ(acclDataXYZ); //retrieve accelerometer readings
    BSP_GYRO_GetXYZ(gyrDataXYZ);
  }
  else
  {
     //retrieve mag readings
    BSP_MAGNETO_GetXYZ(magnDataXYZ);
  }
}

uint16_t *s4380479_get_acc_reading(void)
{
  return acclDataXYZ;
}

uint16_t *s4380479_get_mag_reading(void)
{
  return magnDataXYZ;
}

uint16_t *s4380479_get_gyr_reading(void)
{
  return gyrDataXYZ;
}