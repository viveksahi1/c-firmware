/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_pantilt.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief	 Produces a dynamic PWM  Waveform on pin D6.
 *			 See Section 18 (TIM3), P576 of the STM32F4xx Reference Manual.
 *
 *			NOTE: Refer to lineS 3163 and 4628 of the stm32f4xx_hal_tim.c, and
 *			lines 960 and 974 of stm32f4xx_hal_tim.h files. Refer to pages
 *			102-103 of the STM32F4-Technical-Training.pdf and page 590 of the
 *			STM32F4XX_reference.pdf. It's pretty good.
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *	extern void s4380479_hal_pantilt_init(void) -  init pantilt
 ******************************************************************************
 */

#include <s4380479_hal_pantilt.h>
#include <s4380479_hal_ledbar.h>
#include <math.h>

/**
 *  @brief  Initialises the hardware to generate the PWM wave of 20ms and 1.45ms
 * @param  None
 * @retval None
 */
extern void s4380479_hal_pantilt_init(void) {

	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_OC_InitTypeDef PWMConfig;
	uint16_t PrescalerValue = 0;

	//init LED
	BRD_LEDInit();
	BRD_LEDRedOff();
	BRD_LEDBlueOff();
	BRD_LEDGreenOff();

	//init clocks
	PWM_PIN_CLK();
	__BRD_D6_GPIO_CLK();
	__BRD_D5_GPIO_CLK();

	//configure PAN pin
	GPIO_InitStructure.Pin = PAN_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Alternate = PWM_GPIO_AF;
	HAL_GPIO_Init(BRD_D6_GPIO_PORT, &GPIO_InitStructure);

	PrescalerValue = (uint16_t) (SystemCoreClock / PWM_CLOCKFREQ) - 1;
	//configure timer handler
	TIM_Init_pan_tilt.Instance = PWM_TIMER;
	TIM_Init_pan_tilt.Init.Period = PWM_PERIOD;
	TIM_Init_pan_tilt.Init.Prescaler = PrescalerValue;
	TIM_Init_pan_tilt.Init.ClockDivision = 0;
	TIM_Init_pan_tilt.Init.RepetitionCounter = 0;
	TIM_Init_pan_tilt.Init.CounterMode = TIM_COUNTERMODE_UP;

	//PWM configuration
	PWMConfig.OCMode = TIM_OCMODE_PWM1;
	PWMConfig.Pulse = PWM_PULSEPERIOD;
	PWMConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
	PWMConfig.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	PWMConfig.OCFastMode = TIM_OCFAST_DISABLE;
	PWMConfig.OCIdleState = TIM_OCIDLESTATE_RESET;
	PWMConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;

	//Enable timer for channel
	HAL_TIM_PWM_Init(&TIM_Init_pan_tilt);
	HAL_TIM_PWM_ConfigChannel(&TIM_Init_pan_tilt, &PWMConfig, PAN_CHANNEL);
	//start timer
	HAL_TIM_PWM_Start(&TIM_Init_pan_tilt, PAN_CHANNEL);

	//configure tilt pin
	GPIO_InitStructure.Pin = TILT_PIN;
	HAL_GPIO_Init(BRD_D5_GPIO_PORT, &GPIO_InitStructure);
	HAL_TIM_PWM_ConfigChannel(&TIM_Init_pan_tilt, &PWMConfig, TILT_CHANNEL);
	HAL_TIM_PWM_Start(&TIM_Init_pan_tilt, TILT_CHANNEL);
}

/**
 * Internal generic function for writing an angle 0 -> +-70.
 *
 * @param	type
 * 		pan/tilt
 *
 * 	@param angle
 * 		angle to write to
 */
void pantilt_angle_write(int type, int angle) {

	float pulse_width = 1.45;
	//set angle max limits
	if (angle > 75 || angle < -75) {

		return;
	}
	pulse_width += (angle / 90.0);
	if (type == PAN_MODE) {

		PAN_DC_SET(pulse_width * 50.0);
	} else if (type == TILT_MODE) {

		TILT_DC_SET(pulse_width * 50.0);
	}

}

/*
 * Internal generic functiuon for calculating the angle from duty cycle
 *
 * @param 	type
 * 		pan/tilt
 *
 * 	@return the current angle
 */
int pantilt_angle_read(int type) {

	uint32_t pan_value = PAN_DC_GET();
	uint32_t tilt_value = TILT_DC_GET();
	float pulse_width;

	if (type == PAN_MODE) {

		pulse_width = (pan_value / 50.0) - 1.45;
	} else if (type == TILT_MODE) {

		pulse_width = (tilt_value / 50.0) - 1.45;
	}

	return pulse_width * 90;
}

/*
 * Iternal generic function to control metronome operation
 *
 * @param	mode
 * 		metronome mode
 */
void pantilt_metronome(int mode) {

	int interval = 2;
	char RxChar;
	int angle;

	while (1) {

		 angle = s4380479_hal_pantilt_pan_read();
		//Write Digital 0 bit value
		update_ledbar(angle, 0);
		debug_printf("anglee:%d\n", angle);

		//change firection if required
		if ((s4380479_hal_pantilt_pan_read() >= 40 && mode)) {

			mode = 0;
		} else if ((s4380479_hal_pantilt_pan_read() <= -40 && !mode)) {

			mode = 1;
		}
		//update angle
		if (mode) {

			pantilt_angle_write(0, s4380479_hal_pantilt_pan_read() + 10);
		} else {

			pantilt_angle_write(0, s4380479_hal_pantilt_pan_read() - 10);
		}

		//calculate delay based on interval
		HAL_Delay(interval * 125); // 125 = 1000ms * 10 (deg) / 80(deg)
		update_ledbar(angle, 1);
		BRD_LEDBlueToggle();

		// Receive characters using getc
		RxChar = debug_getc();

		// Check if character is not null
		if (RxChar != '\0') {

#ifdef PRINTF_REFLECT
			debug_printf("Direction: %c\n", RxChar); // reflect byte using printf - must delay before calling printf again
#else
			debug_putc(RxChar); // reflect byte using putc - puts character into buffer
			debug_flush();
#endif
		}
		if (RxChar == 'n') {

			//get angle then move to that angle
			pantilt_angle_write(0, s4380479_hal_pantilt_pan_read() + 10);
			break;
		} else if (RxChar == '+' && interval < 20) {

			interval++;
		} else if (RxChar == '-' && interval > 2) {

			interval--;
		}

	}
}

/*
 *  Iternal generic function to control metronome operation
 *
 *  @param mode
 *  		metronome mode
 *
 *  @param angle
 *  		current angle
 */
void update_ledbar(int angle, int mode) {

	mode = ~mode;
	if (angle >= 35 && angle <= 45) {

		HAL_GPIO_WritePin(BRD_D24_GPIO_PORT, BRD_D24_PIN, mode & 0x01);
	} else if (angle >= 25 && angle <= 35) {

		HAL_GPIO_WritePin(BRD_D23_GPIO_PORT, BRD_D23_PIN, mode & 0x01);
	} else if (angle >= 15 && angle <= 25) {

		HAL_GPIO_WritePin(BRD_D22_GPIO_PORT, BRD_D22_PIN, mode & 0x01);
	} else if (angle >= 05 && angle <= 15) {

		HAL_GPIO_WritePin(BRD_D21_GPIO_PORT, BRD_D21_PIN, mode & 0x01);
	} else if (angle >= -5 && angle <= 05) {

		HAL_GPIO_WritePin(BRD_D20_GPIO_PORT, BRD_D20_PIN, mode & 0x01);
	} else if (angle >= -15 && angle <= -5) {

		HAL_GPIO_WritePin(BRD_D19_GPIO_PORT, BRD_D19_PIN, mode & 0x01);
	} else if (angle >= -25 && angle <= -15) {

		HAL_GPIO_WritePin(BRD_D18_GPIO_PORT, BRD_D18_PIN, mode & 0x01);
	} else if (angle >= -35 && angle <= -25) {

		HAL_GPIO_WritePin(BRD_D17_GPIO_PORT, BRD_D17_PIN, mode & 0x01);
	} else if (angle >= -45 && angle <= -35) {

		HAL_GPIO_WritePin(BRD_D16_GPIO_PORT, BRD_D16_PIN, mode & 0x01);
	}
}

