/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_led.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli led
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_led.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableLed; //variable to store led cli cmds

/**
 * This method allcoates the memory for the argtable to store led cmds.
 */
void allocate_led_argtable()
{
    argtableLed = malloc(sizeof(struct arg_int *) * 3 + sizeof(struct arg_lit *) 
                    + sizeof(struct arg_end *) + sizeof(void *) + sizeof(char) * MEM_OVF_BUF);
}

/**
 * Initialise the time CLI cmds
 */
void s4380479_init_led_cmd() 
{
    /** LED CLI CMD */
    // struct arg_rex *ledCmd = arg_rex1(NULL, NULL, "led", NULL, REG_ICASE, NULL);
    struct arg_int *off = arg_int0("f", "off", "<int>", "Turns 'OFF' the given led on the board.");
    struct arg_int *on = arg_int0("o", "on", "<int>", "Turns 'ON' the given led on the board.");
    struct arg_int *tog = arg_int0("t", "toggle", "<int>", "Toggles the given led on the board.");
    struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *end = arg_end(20);

    //redirect error func   
    off->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    on->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    tog->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    help->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    end->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_led_argtable();
    argtableLed[cOffLed] = off;
    argtableLed[cOnLed] = on;
    argtableLed[cTogLed] = tog;
    argtableLed[cHelpLed] = help;
    argtableLed[cEndLed] = end;
}

/**
 * This method return the artable for the led cmd
 */
void** s4380479_get_led_cmd()
{
    return argtableLed;
}

/**
 * This is the callback function to hadndle led cmds
 */
void handle_led(int off, int offLed, int on,
                int onLed, int tog, int togLed)
{

    if (off)
    {
        s4380479_hal_led(offLed, LED_LOW, 0);
    }
    else if (on)
    {
        s4380479_hal_led(onLed, LED_HIGH, 0);
    }
    else if (tog)
    {
        s4380479_hal_led(togLed, 0, 1); //0 does not affect
    }
}