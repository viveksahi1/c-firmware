/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_ir_coms.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   IR coms peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_hal_manchester_encode(uint8_t msb, uint8_t lsb,
		int encode[], int mode) manchester encodes hex byte input

*  extern int s4380479_hal_manchester_decode(uint8_t input[], int size) manchest
*  	decode a hex input
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <s4380479_hal_ir_coms.h>
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include <s4380479_hal_hamming.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/**
 * This fucntion takes a byte (msb and lsb) and encodes it
 * using manchester 2 format. the encoded data is stored in encode array
 *
 * @param msb
 * 		upper bit of the byte
 *
 * @param lsb
 * 		lower bit of the byte
 * @param encode
 * 		array to store encoded data
 *
 * 	@param mode
 * 		if mode is 1 then print output
 *
 */
extern void s4380479_hal_manchester_encode(uint8_t msb, uint8_t lsb,
		int encode[], int mode) {

	int input_byte[8];	//convert input to binary
	msb = hex_to_int_converter(msb);
	lsb = hex_to_int_converter(lsb);

	//extract bits
	input_byte[3] = !!(msb & 0x1);
	input_byte[2] = !!(msb & 0x2);
	input_byte[1] = !!(msb & 0x4);
	input_byte[0] = !!(msb & 0x8);
	input_byte[7] = !!(lsb & 0x1);
	input_byte[6] = !!(lsb & 0x2);
	input_byte[5] = !!(lsb & 0x4);
	input_byte[4] = !!(lsb & 0x8);

	//start and end bits
	encode[0] = 0;
	encode[1] = 1;
	encode[2] = 0;
	encode[3] = 1;
	encode[20] = 1;
	encode[21] = 0;

	//encode
	for (int i = 0; i < NUM_ENCODE_BITS; i++) {
		if (input_byte[i]) {

			// 1 -> 0,1
			encode[i * 2 + 4] = 0;
			encode[i * 2 + 5] = 1;
		} else {

			// 0 -> 1, 0
			encode[i * 2 + 4] = 1;
			encode[i * 2 + 5] = 0;
		}
	}
	if (mode) {

		printf("%X",
				encode[4] << 3 | encode[5] << 2 | encode[6] << 1 | encode[7]);
		printf("%X",
				encode[8] << 3 | encode[9] << 2 | encode[10] << 1 | encode[11]);
		printf("%X",
				encode[12] << 3 | encode[13] << 2 | encode[14] << 1
						| encode[15]);
		printf("%X\r\n",
				encode[16] << 3 | encode[17] << 2 | encode[18] << 1
						| encode[19]);
	}
}

/**
 * This fucntion takes a int array of bits and decodes it
 * using manchester 2 format. the decodee value is returned as int
 *
 * @param input
 * 		array with binary data
 *
 * @param size
 * 		number of btis in array
 *
 */
extern int s4380479_hal_manchester_decode(uint8_t input[], int size) {

	int input_data[size];	//convert input to binary
	int elements = sizeof(input_data) / (sizeof(int) * 4);
	int result[size / 2]; //stores decode bits
	int i, j = 0;
	uint8_t data;

	/* extract bits */
	for (i = 0; i < elements; i++) {
		//printf("input:%d\r\n", input[i]);
		data = hex_to_int_converter(input[i]);
		input_data[(i * 4) + 3] = !!(data & 0x1);
		input_data[(i * 4) + 2] = !!(data & 0x2);
		input_data[(i * 4) + 1] = !!(data & 0x4);
		input_data[(i * 4) + 0] = !!(data & 0x8);
	}
	//decode using manchester 2
	for (i = 0; i < size; i += 2) {
		if (input_data[i] == 1 && input_data[i + 1] == 0) {

			//decode to 0
			result[j++] = 0;
		} else if (input_data[i] == 0 && input_data[i + 1] == 1) {

			//decode to 1
			result[j++] = 1;
		}
	}

	return result[0] << 7 | result[1] << 6 | result[2] << 5 | result[3] << 4
			| result[4] << 3 | result[5] << 2 | result[6] << 1 | result[7];
}
