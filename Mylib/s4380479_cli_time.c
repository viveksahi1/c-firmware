/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_time.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli time
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_time.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableTime; //variable to store time cli cmds

/**
 * This method allcoates the memory for the argtable to store time cmds.
 */
void allocate_time_argtable()
{
    argtableTime = malloc(sizeof(struct arg_lit *) * 2 + sizeof(struct arg_end *) +
                          sizeof(void *) + sizeof(char) * MEM_OVF_BUF);
}

/**
 * Initialise the time CLI cmds
 */
void s4380479_init_time_cmd()
{
    /** TIME CLI CMD */
    // struct arg_rex *timeCmd = arg_rex1(NULL, NULL, "time", NULL, REG_ICASE, NULL);
    struct arg_lit *sform = arg_lit0("f", "format", "Displays the formatted system power time (hrs:min:sec)");
    struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *end = arg_end(20);

    //redirects the error function
    sform->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    help->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    end->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_time_argtable();
    argtableTime[sformTime] = sform;
    argtableTime[helpTime] = help;
    argtableTime[endTime] = end;
}

/**
 * This method return the artable for the time cmd
 */
void **s4380479_get_time_cmd()
{
    return argtableTime;
}

/**
 * This is the callback function to hadndle time cmds
 */
void get_sys_time(int format)
{
    uint32_t sysRunTime;
    sysRunTime = HAL_GetTick();
    if (format)
    {
        s4380479_os_log_message(DEBUG_MSG, "Formatted time requested");

        s4380479_os_log_message(LOG_MSG, "System Runtime (hr:min:sec)-> %d, %d, %d",
                                ((sysRunTime / (1000 * 60 * 60)) % 24),
                                ((sysRunTime / (1000 * 60)) % 60),
                                ((sysRunTime / 1000) % 60));
    }
    else
    {
        s4380479_os_log_message(DEBUG_MSG, "Systime in seconds requested");
        s4380479_os_log_message(LOG_MSG, "System Runtime (seconds) %d", sysRunTime / 1000);
    }
}
