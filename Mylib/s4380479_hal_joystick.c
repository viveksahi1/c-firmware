/**
 ******************************************************************************
 * @file    mylib/s4380479_hal_joystick.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   joystick peripheral driver
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_hal_joystick_init(void) -initialise joystick
 * int joystick_read(ADC_HandleTypeDef AdcHandle) - reads joystick vcalue
 * int joystick_value(int adc) - convert adc to joystick value
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <s4380479_hal_joystick.h>
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/**
 * @brief  Initialise Joystick.
 * @param  None
 * @retval None
 */
extern void s4380479_hal_joystick_init(void) {

	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_ChannelConfTypeDef AdcChanConfig;
	ADC_ChannelConfTypeDef AdcChanConfig_y;

	/* enable clocks */
	__BRD_A0_GPIO_CLK();
	__BRD_A1_GPIO_CLK();
	__ADC1_CLK_ENABLE();
	__ADC2_CLK_ENABLE();

	//A0 for joystick x
	GPIO_InitStructure.Pin = BRD_A0_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BRD_A0_GPIO_PORT, &GPIO_InitStructure);

	//ADC_x init
	AdcHandle_x.Instance = (ADC_TypeDef *) (ADC1_BASE);				//Use ADC1
	AdcHandle_x.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;	//Set clock prescaler
	AdcHandle_x.Init.Resolution = ADC_RESOLUTION12b;//Set 12-bit data resolution
	AdcHandle_x.Init.ScanConvMode = DISABLE;
	AdcHandle_x.Init.ContinuousConvMode = DISABLE;
	AdcHandle_x.Init.DiscontinuousConvMode = DISABLE;
	AdcHandle_x.Init.NbrOfDiscConversion = 0;
	AdcHandle_x.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;//No Trigger
	AdcHandle_x.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1;//No Trigger
	AdcHandle_x.Init.DataAlign = ADC_DATAALIGN_RIGHT;		//Right align data
	AdcHandle_x.Init.NbrOfConversion = 1;
	AdcHandle_x.Init.DMAContinuousRequests = DISABLE;
	AdcHandle_x.Init.EOCSelection = DISABLE;

	HAL_ADC_Init(&AdcHandle_x);		//Initialise ADC_x
	/* Configure ADC_x Channel */
	AdcChanConfig.Channel = BRD_A0_ADC_CHAN;						//Use AO pin
	AdcChanConfig.Rank = 1;
	AdcChanConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
	AdcChanConfig.Offset = 0;
	HAL_ADC_ConfigChannel(&AdcHandle_x, &AdcChanConfig);

	//A1 for joystick y
	GPIO_InitStructure.Pin = BRD_A1_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BRD_A1_GPIO_PORT, &GPIO_InitStructure);

	//ADC_x init
	AdcHandle_y.Instance = (ADC_TypeDef *) (ADC2_BASE);				//Use ADC1
	AdcHandle_y.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;	//Set clock prescaler
	AdcHandle_y.Init.Resolution = ADC_RESOLUTION12b;//Set 12-bit data resolution
	AdcHandle_y.Init.ScanConvMode = DISABLE;
	AdcHandle_y.Init.ContinuousConvMode = DISABLE;
	AdcHandle_y.Init.DiscontinuousConvMode = DISABLE;
	AdcHandle_y.Init.NbrOfDiscConversion = 0;
	AdcHandle_y.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;//No Trigger
	AdcHandle_y.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1;//No Trigger
	AdcHandle_y.Init.DataAlign = ADC_DATAALIGN_RIGHT;		//Right align data
	AdcHandle_y.Init.NbrOfConversion = 1;
	AdcHandle_y.Init.DMAContinuousRequests = DISABLE;
	AdcHandle_y.Init.EOCSelection = DISABLE;

	HAL_ADC_Init(&AdcHandle_y);		//Initialise ADC
	/* Configure ADC_y Channel */
	AdcChanConfig_y.Channel = BRD_A1_ADC_CHAN;						//Use A1 pin
	AdcChanConfig_y.Rank = 1;
	AdcChanConfig_y.SamplingTime = ADC_SAMPLETIME_3CYCLES;
	AdcChanConfig_y.Offset = 0;
	HAL_ADC_ConfigChannel(&AdcHandle_y, &AdcChanConfig_y);
}

/**
 * @brief  Reads the adc value and return it
 *
 * @param  value
 * @retval None
 */
int joystick_read(ADC_HandleTypeDef AdcHandle) {

	unsigned int adc_value;
	/* convert */
	HAL_ADC_Start(&AdcHandle); // Start ADC conversion
	// Wait for ADC conversion to finish
	while (HAL_ADC_PollForConversion(&AdcHandle, 10) != HAL_OK);
	adc_value = (uint16_t) (HAL_ADC_GetValue(&AdcHandle));

	return adc_value; // * 3.3 / 4095) * 10;
}

/**
 * Convert the adc value to joystick volatge
 *
 * @param adc value
 *
 * @return joystick voltage
 */
int joystick_value(int adc) {

	return (adc * 3.3 / 4095) * 10;
}

