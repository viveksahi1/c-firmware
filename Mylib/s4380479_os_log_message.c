/**
 ******************************************************************************
 * @file    myoslib/s4380479_os_printf.c
 * @author  Vivek Sahi - 43804791
 * @date    11-03-2019
 * @brief   OS printf drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskPrintf(void) - task to receive from printf
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_os_printf.h"
#include "s4380479_os_log_message.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define LOG_MESSAGE_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 4)
#define LOG_MESSAGE_PRIORITY (tskIDLE_PRIORITY + 4)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char *dataPtr; // variable to store the data being sent into queue

/**
 * Initialises the pins used for the system monitor
 */
void s4380479_TaskLogMessage(void)
{
    struct LogMessage logMsg;
    int logType = CLI_ALL;

    while (1)
    {

        if (s4380479_QueueLogMessage != NULL)
        {

            if (xQueueReceive(s4380479_QueueLogMessage, &logMsg, __UINT32_MAX__))
            {

                // update error message type according to message received
                if (logMsg.type == ERROR_MSG && (logType == CLI_ERROR || logType == CLI_ALL))
                {
                    s4380479_os_printf(ANSI_COLOR_RED "\rERROR: %s\r", logMsg.messageRec);
                    s4380479_os_printf(ANSI_COLOR_RESET);
                }
                else if (logMsg.type == LOG_MSG && (logType == CLI_LOG || logType == CLI_ALL))
                {
                    s4380479_os_printf(ANSI_COLOR_GREEN "\rLOG: %s\r", logMsg.messageRec);
                    s4380479_os_printf(ANSI_COLOR_RESET);
                }
                else if (logMsg.type == DEBUG_MSG && (logType == CLI_DEBUG || logType == CLI_ALL))
                {
                    s4380479_os_printf(ANSI_COLOR_CYAN "\rDEBUG: %s\r", logMsg.messageRec);
                    s4380479_os_printf(ANSI_COLOR_RESET);
                }

                else if (logMsg.type == CLI_DEBUG)
                {
                    logType = CLI_DEBUG;
                }

                else if (logMsg.type == CLI_LOG)
                {
                    logType = CLI_LOG;
                }

                else if (logMsg.type == CLI_ERROR)
                {
                    logType = CLI_ERROR;
                }
                else if (logMsg.type == CLI_ALL)
                {
                    logType = CLI_ALL;
                }
            }
        }
    }
}

/** task safe printf function */
void s4380479_os_log_message(int messageType, char *message, ...)
{

    struct LogMessage logMsg;

    logMsg.type = messageType;

    //receive arguments
    va_list args;
    va_start(args, message);

    dataPtr = logMsg.messageRec;
    vsprintf(dataPtr, message, args);
    va_end(args);

    // sprintf(logMsg.messageRec, "%s",message);

    if (xQueueSend(s4380479_QueueLogMessage, (void *)&logMsg,
                   (portTickType)100) != pdPASS)
    {
        //failed
        s4380479_os_printf("failied to send log queue %s", logMsg.messageRec);
    } 
}

/**
 * Initialises the task for logging error, log and debug messages
 */
void s4380479_os_log_message_init(void)
{
    //init Queue
    s4380479_QueueLogMessage = xQueueCreate(10, sizeof(struct LogMessage));
    
    if (xTaskCreate((TaskFunction_t)&s4380479_TaskLogMessage, "LOGMESSAGE", LOG_MESSAGE_TASK_STACK_SIZE, NULL,
                    LOG_MESSAGE_PRIORITY, NULL) != pdPASS)
    {
        s4380479_os_printf("%s\r\n", "failed to create log message task");
    }
}
