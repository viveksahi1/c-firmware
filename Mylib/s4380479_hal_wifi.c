/**
 ******************************************************************************
6* @file    myoslib/s4380479_hal_wifi.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal wifi drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_wifi.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
// #define SSID "Dodo"
// #define PASSWORD "1notebook"
#define SSID "hypnos-blue"
#define PASSWORD "hypnos-blue52"
#define PORT 10004
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t MAC_Addr[6];
uint8_t IP_Addr[4];
uint16_t Datalen;

uint8_t RemoteIP[] = {192,168,43,240};
uint8_t TxData[] = "{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}{\"data\":{\"A\":{\"RSSI\": 75, \"Ultra\": 300}, \"B\":{\"RSSI\": 40, \"Ultra\": 150},\"C\":{\"RSSI\": 40, \"Ultra\": 300},\"D\":{\"RSSI\": 40, \"Ultra\": 100}, \"Acc\":{\"x\": 100, \"y\": 400, \"z\":34}, \"Gyro\":{\"x\": 100, \"y\": 400, \"z\":34}}}";

uint8_t RxData[500];

uint16_t respLen;
int test_temp = 35;
uint16_t Trials = 100;

int32_t Socket = -1;

WebServerState_t State = WS_ERROR;
/* External function prototypes -----------------------------------------------*/

void s4308479_init_wifi(void)
{
    /*Initialize and use WIFI module */
    if (WIFI_Init() == WIFI_STATUS_OK)
    {
        printf("ES-WIFI Initialized. \r\n");

        if (WIFI_GetMAC_Address(MAC_Addr) == WIFI_STATUS_OK)
        {
            printf("> es-wifi module MAC Address : %X:%X:%X:%X:%X:%X \r\n",
                   MAC_Addr[0],
                   MAC_Addr[1],
                   MAC_Addr[2],
                   MAC_Addr[3],
                   MAC_Addr[4],
                   MAC_Addr[5]);
        }
        else
        {
            printf("> ERROR : CANNOT get MAC address \r\n");
        }
    }
    else
    {
        printf("> ERROR : WIFI Module cannot be initialized. \r\n");
    }
}

void s4380479_wifi_connect(void)
{
    
    if (WIFI_Connect(SSID, PASSWORD, WIFI_ECN_WPA2_PSK) == WIFI_STATUS_OK)
    {
        printf("> es-wifi module connected  \r\n");

        if (WIFI_GetIP_Address(IP_Addr) == WIFI_STATUS_OK)
        {
            printf("> es-wifi module got IP Address : %d.%d.%d.%d \r\n",
                   IP_Addr[0],
                   IP_Addr[1],
                   IP_Addr[2],
                   IP_Addr[3]);

            printf("> Trying to connect to Server: %d.%d.%d.%d:%d ...\n",
                   RemoteIP[0],
                   RemoteIP[1],
                   RemoteIP[2],
                   RemoteIP[3], PORT);

            //ATTEMPT CONNECTION
            while (Trials--)
            {
                if (WIFI_OpenClientConnection(0, WIFI_TCP_PROTOCOL, "TCP_CLIENT", RemoteIP, PORT, 0) == WIFI_STATUS_OK)
                {
                    printf("> TCP Connection opened successfully.\n");
                    Socket = 0;
                    break;
                }
                // s4380479_WebServer_Process();
            }
            if (!Trials)
            {
                printf("> ERROR : Cannot open Connection\n");
                // BSP_LED_On(LED2);
            }

            // State = WS_IDLE;
        }
        else
        {
            printf("> ERROR : es-wifi module CANNOT get IP address \r\n");
        }
    }
    else
    {

        printf("> ERROR : es-wifi module NOT connected \r\n");
    }
}

void s4380479_WebServer_Process(void)
{
    printf("running...");
    if (Socket != -1)
    {
        //  s4380479_os_log_message(LOG_MSG, "SOCKET is 0");
        if (WIFI_SendData(Socket, TxData, strlen(TxData), &Datalen, WIFI_WRITE_TIMEOUT) != WIFI_STATUS_OK)
        {
            printf("> ERROR : Failed to send Data.\r\n");
        } else {
            printf("data sent\r\n");
        }
        HAL_Delay(10);
    }
}


void s4380479_Wifi_Process(uint8_t *data)
{
    if (Socket != -1)
    {
        //  s4380479_os_log_message(LOG_MSG, "SOCKET is %d",strlen(data) );
        if (WIFI_SendData(Socket, data, strlen(data), &Datalen, WIFI_WRITE_TIMEOUT) != WIFI_STATUS_OK)
        {
            // s4380479_os_log_message(LOG_MSG, "> ERROR : Failed to send Data.\n");
        }
        HAL_Delay(1);
    }
}