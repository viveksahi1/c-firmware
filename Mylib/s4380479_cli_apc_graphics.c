/**
 ******************************************************************************
 * @file    mylib/s4380479_cli_apc_shell.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   apc shell commands drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * s4380479_cli_apc_shell_init(void); - init apc shell commands
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "nrf24l01plus.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "FreeRTOS_CLI.h"
#include "s4380479_cli_apc_shell.h"
#include "string.h"
#include "s4380479_os_radio.h"

uint32_t systemInitTime;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

CLI_Command_Definition_t xOrigin = { /* Structure that defines the "origin" command line command. */
"origin", "origin: Moves plotter head to origin\r\n", prvOrigin, 0 };

CLI_Command_Definition_t xLine =
		{ /* Structure that defines the "line" command line command. */"line",
				"line: <x1> <y1> <type h/v> <length>  Draws a horizontal or vertical line\r\n",
				prvLine, 4 };

CLI_Command_Definition_t xSquare =
		{ /* Structure that defines the "square" command line command. */
		"square", "square: <x1> <y1> <side> Draws a square from (x,y)\r\n",
				prvSquare, 3 };

CLI_Command_Definition_t xBline =
		{ /* Structure that defines the "bline" command line command. */
		"bline",
				"bline: <x1> <y1> <x2> <y2> <step size> Draws line using Bresenham line algorithm \r\n",
				prvBline, 5};

/**
 * @brief  Moves plotter head to origin
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvOrigin(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	uint32_t sysRunTime = HAL_GetTick() - systemInitTime;

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "%s\n\r", "");

	xSemaphoreGive(s4380479_SemaphoreOrigin);
	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * line cmd call back
 */
static BaseType_t prvLine(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long xParam_len, yParam_len, typeParam_len, lengthParam_len;
	char *xInput, *yInput, *type, *length;
	struct DrawLine msg;

	/* Get parameters from command string */
	xInput = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParam_len);
	yInput = FreeRTOS_CLIGetParameter(pcCommandString, 2, &yParam_len);
	type = FreeRTOS_CLIGetParameter(pcCommandString, 3, &typeParam_len);
	length = FreeRTOS_CLIGetParameter(pcCommandString, 4, &lengthParam_len);

	xInput[xParam_len] = 0x00;
	yInput[yParam_len] = 0x00;
	type[typeParam_len] = 0x00;
	length[lengthParam_len] = 0x00;

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s %s\n\r", xInput,
			yInput);

	if (s4380479_radio_is_number(xInput) && s4380479_radio_is_number(yInput)
			&& s4380479_radio_is_number(length)
			&& (!strcmp(type, "h") || !strcmp(type, "v"))) {

		draw_line(atoi(xInput), atoi(yInput), type[0], atoi(length));
	}

	return pdFALSE;
}

/**
 * draws a square with given dimesnsions
 */
static BaseType_t prvSquare(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long xParam_len, yParam_len, sideParam_len;
	char *xInput, *yInput, *side;
	int x, y, length;

	/* Get parameters from command string */
	xInput = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParam_len);
	yInput = FreeRTOS_CLIGetParameter(pcCommandString, 2, &yParam_len);
	side = FreeRTOS_CLIGetParameter(pcCommandString, 3, &sideParam_len);

	xInput[xParam_len] = 0x00;
	yInput[yParam_len] = 0x00;
	side[sideParam_len] = 0x00;

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s %s\n\r", xInput,
			yInput);

	if (s4380479_radio_is_number(xInput) && s4380479_radio_is_number(yInput)
			&& s4380479_radio_is_number(side)) {

		x = atoi(xInput), y = atoi(yInput), length = atoi(side);
		if (draw_line(x, y, 'h', length)) {
			vTaskDelay(100);
			move_position(x + length, y - length);
			vTaskDelay(100);
			move_position(x, y - length);
			vTaskDelay(100);
			move_position(x, y);
			vTaskDelay(100);
		}

	}

	return pdFALSE;
}

/**
 * Bline cmd callback
 */
static BaseType_t prvBline(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long xParam_len1, yParam_len1, xParam_len2, yParam_len2, sizeParam_len;
	char *xInput1, *yInput1, *xInput2, *yInput2, *size;
	//int x1, y1, x2, y2, step;

	/* Get parameters from command string */
	xInput1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParam_len1);
	yInput1 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &yParam_len1);
	xInput2 = FreeRTOS_CLIGetParameter(pcCommandString, 3, &xParam_len2);
	yInput2 = FreeRTOS_CLIGetParameter(pcCommandString, 4, &yParam_len2);
	size = FreeRTOS_CLIGetParameter(pcCommandString, 5, &sizeParam_len);

	/* extratc inouts */
	xInput1[xParam_len1] = 0x00;
	yInput1[yParam_len1] = 0x00;
	xInput2[xParam_len2] = 0x00;
	yInput2[yParam_len2] = 0x00;
	size[sizeParam_len] = 0x00;

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "%s\n\r", "");

	if (s4380479_radio_is_number(xInput1) && s4380479_radio_is_number(yInput1)
			&& s4380479_radio_is_number(xInput2)
			&& s4380479_radio_is_number(yInput2)
			&& s4380479_radio_is_number(size)) {

		draw_bline(atoi(xInput1), atoi(yInput1), atoi(xInput2), atoi(yInput2),
				atoi(size));
	}
	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * Initialises the APC shell commands
 */
extern void s4380479_cli_apc_graphics_init(void) {

	FreeRTOS_CLIRegisterCommand(&xOrigin);
	FreeRTOS_CLIRegisterCommand(&xLine);
	FreeRTOS_CLIRegisterCommand(&xSquare);
	FreeRTOS_CLIRegisterCommand(&xBline);
}

