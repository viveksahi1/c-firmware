/**
 ******************************************************************************
6* @file    myoslib/s4380479_hal_qspi.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal qspi drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_qspi.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define QSPI_BUFFER_SIZE ((uint32_t)0x0100)
#define WRITE_READ_ADDR ((uint32_t)0x0050)
#define HEADBAND_HEIGHT 64
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t qspi_aTxBuffer[QSPI_BUFFER_SIZE] = "HELLO WORLD!";
uint8_t qspi_aRxBuffer[500];

/* External function prototypes -----------------------------------------------*/
static void Fill_Buffer(uint8_t *pBuffer, uint32_t uwBufferLength, uint32_t uwOffset);
static uint8_t Buffercmp(uint8_t *pBuffer1, uint8_t *pBuffer2, uint32_t BufferLength);

spiffs_config cfg;
int32_t res;

spiffs fs;

uint8_t FS_Work_Buf[256 * 2];
uint8_t FS_FDS[32 * 4];
uint8_t FS_Cache_Buf[(256 + 32) * 4];

uint32_t total = 0;
uint32_t used_space = 0;

char cwd[100] = "";

static int32_t _spiffs_erase(uint32_t addr, uint32_t len)
{
    uint32_t i = 0;
    uint32_t erase_count = (len + 4096 - 1) / 4096;
    for (i = 0; i < erase_count; i++)
    {
        BSP_QSPI_Erase_Block(addr + i * 4096);
    }
    return 0;
}

static int32_t _spiffs_read(uint32_t addr, uint32_t size, uint8_t *dst)
{
    BSP_QSPI_Read(dst, addr, size);
    return 0;
}

static int32_t _spiffs_write(uint32_t addr, uint32_t size, uint8_t *dst)
{
    BSP_QSPI_Write(dst, addr, size);
    return 0;
}

void s4380479_init_fileSystem(void)
{
    BSP_QSPI_Init();
    printf("!!!!!!!!!! INIT DONE! CREATE File System !!!!!!!!\r\n");

    cfg.hal_erase_f = _spiffs_erase;
    cfg.hal_read_f = _spiffs_read;
    cfg.hal_write_f = _spiffs_write;

    if ((res = SPIFFS_mount(&fs,
                            &cfg,
                            FS_Work_Buf,
                            FS_FDS, sizeof(FS_FDS),
                            FS_Cache_Buf, sizeof(FS_Cache_Buf),
                            NULL)) != SPIFFS_OK &&
        SPIFFS_errno(&fs) == SPIFFS_ERR_NOT_A_FS)
    {
        printf("formatting spiffs...\r\n");
        if (SPIFFS_format(&fs) != SPIFFS_OK)
        {
            printf("SPIFFS format failed: %d\r\n", SPIFFS_errno(&fs));
        }
        printf("ok\r\n");
        printf("mounting\r\n");
        res = SPIFFS_mount(&fs,
                           &cfg,
                           FS_Work_Buf,
                           FS_FDS, sizeof(FS_FDS),
                           FS_Cache_Buf, sizeof(FS_Cache_Buf),
                           NULL);
    }
    if (res != SPIFFS_OK)
    {
        printf("SPIFFS mount failed: %d\r\n", SPIFFS_errno(&fs));
    }
    else
    {
        printf("SPIFFS mounted\r\n");
    }
    // open/create file
    // s4380479_create_file("root_file");
    // s4380479_create_file("dir1/dir_file");
    // s4380479_create_file("dir1/dir2/dir2_file");

    // read file
    // s4380479_read_file("nest2_file");
    // s4380479_read_file("nest1/nest1_file");

    // remove file
    // s4380479_remove_file("dir1dir2_file");
    // s4380479_remove_file("nest1/nest2/nest2_file");
    // s4380479_remove_file("nest2/sample_file");

    // s4380479_tree();
    // s4380479_move_file("root_file", "dir1");
    // s4380479_tree();
    // s4380479_cd_dir("dir1");
    // s4380479_move_file("dir_file", "dir2");
    // s4380479_tree();
    // // s4380479_list_file(cwd);

    // s4380479_cd_dir("dir2");
    // s4380479_move_file("dir2_file", "dir1");
    // // s4380479_list_file(cwd);

    // // s4380479_move_file("sample_file", "");
    // // s4380479_list_file(cwd);
    // s4380479_tree();
}

void s4380479_tree(void)
{

    spiffs_DIR d;
    struct spiffs_dirent e;
    struct spiffs_dirent *pe = &e;

    SPIFFS_opendir(&fs, "", &d);
    while ((pe = SPIFFS_readdir(&d, pe)))
    {
        s4380479_os_log_message(DEBUG_MSG, "--->%s\r\n", (char *)pe->name);
    }
    SPIFFS_closedir(&d);
    HAL_Delay(1000);
}

void s4380479_create_dir(char *dirName)
{
    char buffer[100];
    sprintf(buffer, "%s/testFile", dirName);
    s4380479_create_file(buffer);
}

void s4380479_remove_dir(char *dirName)
{
    spiffs_DIR d;
    struct spiffs_dirent e;
    struct spiffs_dirent *pe = &e;

    SPIFFS_opendir(&fs, "", &d);
    while ((pe = SPIFFS_readdir(&d, pe)))
    {
        if (strstr((char *)pe->name, dirName) != NULL)
        {
            s4380479_os_log_message(LOG_MSG, "Cleaning DIR:%s\r\n", dirName);
            SPIFFS_remove(&fs, (char *)pe->name);
            s4380479_os_log_message(LOG_MSG, "Removed:%s\r\n", (char *)pe->name);
        }
    }
    SPIFFS_closedir(&d);
    HAL_Delay(1000);
}

void s4380479_move_file(char *file, char *moveDir)
{

    char *currentDir;
    char newName[100], oldName[100], tmp_file[100];
    if (s4380479_file_exists(file))
    {
        // s4380479_os_log_message(ERROR_MSG, "leftdir%s-%s\r\n", file, moveDir);
        sprintf(oldName, "%s%s", cwd, file);
        if ((currentDir = strstr(oldName, moveDir)) != NULL)
        { //left dir
            // s4380479_os_log_message(LOG_MSG, "leftdir%s-%s\r\n", file, moveDir);

            if (!(currentDir - oldName))
            {
                sprintf(newName, "%s/%s", moveDir, file);
                // s4380479_os_log_message(LOG_MSG, "leftdir%s-%s\r\n", oldName, newName);
                SPIFFS_rename(&fs, oldName, newName);
                if (s4380479_file_exists(oldName))
                {
                    SPIFFS_remove(&fs, oldName);
                }
            }
            //  s4380479_os_log_message(LOG_MSG, "%s-%s\r\n", oldName, newName);
        }
        else if (s4380479_next_dir(moveDir))
        { //next dir
            sprintf(tmp_file, oldName);
            // sprintf(oldName, "%s%s", moveDir, file);
            if (strrchr(oldName, '/') != NULL)
            {
                // s4380479_os_log_message(LOG_MSG, "nextdir%s-%s\r\n", file, moveDir);
                oldName[strrchr(oldName, '/') - oldName] = '\0';
                sprintf(newName, "%s/%s/%s", oldName, moveDir, file);
                SPIFFS_rename(&fs, tmp_file, newName);
            }
            else
            {
                // s4380479_os_log_message(LOG_MSG, "%s-%s\r\n", file, moveDir);
                sprintf(newName, "%s/%s", moveDir, file);
                SPIFFS_rename(&fs, tmp_file, newName);
            }
        }
        else
        {
            s4380479_os_log_message(LOG_MSG, "[%s] is either current dir or it DNE\r\n", moveDir);
        }
    }
    else
    {
        s4380479_os_log_message(LOG_MSG, "Error FIle Does not Exist!\r\n");
    }
}

void s4380479_create_file(char *filename)
{
    char filePath[100];
    sprintf(filePath, "%s%s", cwd, filename);
    //create file and write
    spiffs_file fd = SPIFFS_open(&fs, filePath, SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
    
    if (SPIFFS_write(&fs, fd, qspi_aTxBuffer, strlen(qspi_aTxBuffer)) < 0)
    {
        s4380479_os_log_message(ERROR_MSG, "errno %d\n", SPIFFS_errno(&fs));
    } else {
        s4380479_os_log_message(DEBUG_MSG, "Created Succesfully");
    }
    SPIFFS_close(&fs, fd);
}

/**
 * Open file for real time logging
 */
signed short s4380479_open_log_file(char *filename)
{
    char filePath[100];
    sprintf(filePath, "%s%s", cwd, filename);
    //create file and write
    s4380479_os_log_message(LOG_MSG, "Creating log file: %s", filePath);
    return SPIFFS_open(&fs, filePath, SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
}

void s4380479_clean_file(signed short fd) {

    if (SPIFFS_write(&fs, fd, "", sizeof(qspi_aRxBuffer)) < 0)
    {
        s4380479_os_log_message(ERROR_MSG, "ERROR WRITING TO LOG FILE");
    }
    // SPIFFS_fflush(&fs, fd);
    SPIFFS_lseek(&fs, fd, 0, SPIFFS_SEEK_SET);
}

/**
 * close openeed file
 */
void s4380479_close_log_file(signed short fd)
{
        s4380479_os_log_message(LOG_MSG, "Closing log file:%d", SPIFFS_close(&fs, fd));

    // SPIFFS_close(&fs, fd);
}

/**
 * Write to Log file
 */
void s4380479_write_log_file(signed short fd, char* data)
{
    //  s4380479_os_log_message(DEBUG_MSG, "writing to file:%s",data);
    if (SPIFFS_write(&fs, fd, data, strlen(data)) < 0)
    {
        s4380479_os_log_message(ERROR_MSG, "ERROR WRITING TO LOG FILE");
    }
    // SPIFFS_fflush(&fs, fd);
}

char *s4380479_check_file_in_dir(char *dir)
{
    return strstr(dir, "/");
}

void s4380479_cd_dir(char *newDir)
{
    if ((strcmp(newDir, "..") == 0) && (strlen(cwd) != 0))
    { // reverse

        //  s4380479_os_log_message(LOG_MSG, "cwd: %s index:%d rev:%d->%d\r\n", cwd, strrchr(cwd, '/') - cwd, cwd -strchr(cwd, '/'), cwd-strrchr(cwd, '/'));
        cwd[strrchr(cwd, '/') - cwd] = '\0';
        if (strrchr(cwd, '/') == NULL)
        {
            cwd[0] = '\0';
        }
        else
        {
            cwd[strrchr(cwd, '/') - cwd + 1] = '\0';
        }
    }
    else if (s4380479_next_dir(newDir))
    {
        sprintf(cwd, "%s%s/", cwd, newDir);
    }
    else
    {
        s4380479_os_log_message(LOG_MSG, "Cannot find directory:%s\r\n", newDir);
    } 
    //printf current dir
    if (strcmp(cwd, "") == 0)
    {
        s4380479_os_log_message(LOG_MSG, "Current Dir: (root)\r\n");
    }
    else
    {
        s4380479_os_log_message(LOG_MSG, "Current Dir: %s\r\n", cwd);
    }
}

int s4380479_next_dir(char *checkNext)
{
    spiffs_DIR d;
    struct spiffs_dirent e;
    struct spiffs_dirent *pe = &e;
    char *nextDir = NULL;
    int status = 0;

    SPIFFS_opendir(&fs, cwd, &d);
    while ((pe = SPIFFS_readdir(&d, pe)))
    {
        if ((nextDir = strstr((char *)pe->name, cwd)) != NULL)
        {
            if (strcmp(cwd, "") == 0)
            {
                if (s4380479_check_file_in_dir(nextDir + strlen(cwd)) != NULL)
                {
                    if (!(nextDir[0] == '/'))
                    {
                        if (strcmp(checkNext, strtok(nextDir, "/")) == 0)
                        {
                            status = 1;
                            break;
                        }
                    }
                }
            }
            else
            {
                if ((s4380479_check_file_in_dir(nextDir + strlen(cwd))) != NULL)
                {
                    nextDir = strstr(nextDir, "/"); //strtok("/dir/", "/") -> dir
                    if (strcmp(checkNext, strtok(nextDir, "/")) == 0)
                    {
                        status = 1;
                        break;
                    }
                }
            }
        }
    }
    SPIFFS_closedir(&d);
    HAL_Delay(1000);
    return status;
}

void s4380479_list_file(void)
{
    spiffs_DIR d;
    struct spiffs_dirent e;
    struct spiffs_dirent *pe = &e;
    char *currentFile;

    SPIFFS_opendir(&fs, "", &d);
    while ((pe = SPIFFS_readdir(&d, pe)))
    {
        if (strcmp(cwd, "") == 0)
        {
            if (s4380479_check_file_in_dir((char *)pe->name) == NULL)
            {
                s4380479_os_log_message(LOG_MSG, "%s\r\n", (char *)pe->name);
            }
        }
        else if ((currentFile = strstr((char *)pe->name, cwd)) != NULL)
        {
            currentFile = s4380479_check_file_in_dir(currentFile + strlen(cwd) - 1);
            if (s4380479_check_file_in_dir(currentFile + 1) == NULL)
            {
                s4380479_os_log_message(LOG_MSG, "[%s]\r\n", currentFile);
            }
        }
    }
    SPIFFS_closedir(&d);
    HAL_Delay(1000);
}

void s4380479_read_file(char *fileName)
{
    char filePath[100];
    sprintf(filePath, "%s%s", cwd, fileName);

    memset(0 , qspi_aRxBuffer, sizeof(qspi_aRxBuffer)); // clean buffer
    //open file and read
    spiffs_file fd = SPIFFS_open(&fs, filePath, SPIFFS_RDONLY, 0);
    if (SPIFFS_read(&fs, fd, qspi_aRxBuffer, sizeof(qspi_aRxBuffer)) < 0)
    {
        s4380479_os_log_message(ERROR_MSG, "Error Reading FIle: %d\r\n", SPIFFS_errno(&fs));
    }
    else
    {
        // s4380479_os_log_message(LOG_MSG, "FileContent:%d\r\n", strlen(qspi_aRxBuffer));
        s4380479_os_log_message(LOG_MSG, "FileContent:%s", qspi_aRxBuffer);
    }

    SPIFFS_close(&fs, fd);
    HAL_Delay(1000);
}

int s4380479_file_exists(char *fileName)
{

    char filePath[100];
    int status = 0;
    sprintf(filePath, "%s%s", cwd, fileName);

    //open file and read
    spiffs_file fd = SPIFFS_open(&fs, filePath, SPIFFS_RDWR, 0);
    if (SPIFFS_read(&fs, fd, qspi_aRxBuffer, sizeof(qspi_aRxBuffer)) < 0)
    {
    }
    else
    {
        status = 1;
    }

    SPIFFS_close(&fs, fd);
    HAL_Delay(1000);
    return status;
}

void s4380479_remove_file(char *fileName)
{
    char filePath[100];
    sprintf(filePath, "%s%s", cwd, fileName);
    SPIFFS_remove(&fs, filePath);
    s4380479_os_log_message(LOG_MSG, "Removed:%s\r\n", fileName);
}

void qspi_test(void)
{

    /* QSPI info structure */
    static QSPI_Info pQSPI_Info;
    uint8_t status;

    BSP_QSPI_DeInit();

    /*##-1- Configure the QSPI device ##########################################*/
    /* QSPI device configuration */
    status = BSP_QSPI_Init();
    if ((status == QSPI_NOT_SUPPORTED) || (status == QSPI_ERROR))
    {
        s4380479_os_log_message(LOG_MSG, "QSPI Init : FAILED, QSPI Test Aborted \r\n");
    }
    else
    {
        printf("QSPI Init : OK \r\n");

        //##-2- Read & check the QSPI info #######################################
        // Initialize the structure
        pQSPI_Info.FlashSize = (uint32_t)0x00;
        pQSPI_Info.EraseSectorSize = (uint32_t)0x00;
        pQSPI_Info.EraseSectorsNumber = (uint32_t)0x00;
        pQSPI_Info.ProgPageSize = (uint32_t)0x00;
        pQSPI_Info.ProgPagesNumber = (uint32_t)0x00;

        // Read the QSPI memory info
        BSP_QSPI_GetInfo(&pQSPI_Info);

        //     / Test the correctness
        if ((pQSPI_Info.FlashSize != 0x800000) || (pQSPI_Info.EraseSectorSize != 0x1000) ||
            (pQSPI_Info.ProgPageSize != 0x100) || (pQSPI_Info.EraseSectorsNumber != 2048) ||
            (pQSPI_Info.ProgPagesNumber != 32768))
        {
            printf("QSPI GET INFO : FAILED, QSPI Test Aborted \r\n");
        }
        else
        {
            printf("QSPI GET INFO : OK \r\n");

            //##-3- Erase QSPI memory ################################################
            if (BSP_QSPI_Erase_Block(WRITE_READ_ADDR) != QSPI_OK)
            {
                printf("QSPI ERASE : FAILED, QSPI Test Aborted \r\n");
            }
            else
            {
                printf("QSPI ERASE : OK \r\n");

                //##-4- QSPI memory read/write access  #################################
                // Fill the buffer to write
                // Fill_Buffer(qspi_aTxBuffer, QSPI_BUFFER_SIZE, 0xD20F);

                // Write data to the QSPI memory
                if (BSP_QSPI_Write(qspi_aTxBuffer, WRITE_READ_ADDR, QSPI_BUFFER_SIZE) != QSPI_OK)
                {
                    printf("QSPI WRITE : FAILED, QSPI Test Aborted \r\n");
                }
                else
                {
                    printf("QSPI WRITE : OK \r\n");

                    // Read back data from the QSPI memory
                    if (BSP_QSPI_Read(qspi_aRxBuffer, WRITE_READ_ADDR, QSPI_BUFFER_SIZE) != QSPI_OK)
                    {
                        printf("QSPI READ : FAILED, QSPI Test Aborted \r\n");
                    }
                    else
                    {
                        printf("QSPI READ : OK \r\n");
                        printf("READ BUFFER%s\r\n", qspi_aRxBuffer);

                        //##-5- Checking data integrity ############################################
                        if (Buffercmp(qspi_aRxBuffer, qspi_aTxBuffer, QSPI_BUFFER_SIZE) > 0)
                        {
                            printf("QSPI COMPARE : FAILED, QSPI Test Aborted \r\n");
                        }
                        else
                        {
                            printf("QSPI Test : OK \r\n");
                        }
                    }
                }
            }
        }
    }
}

/**
* @brief  Fills buffer with user predefined data.
* @param  pBuffer: pointer on the buffer to fill
* @param  uwBufferLenght: size of the buffer to fill
* @param  uwOffset: first value to fill on the buffer
* @retval None
*/
static void Fill_Buffer(uint8_t *pBuffer, uint32_t uwBufferLenght, uint32_t uwOffset)
{
    uint32_t tmpIndex = 0;
    char test = '!';

    /* Put in global buffer different values */
    for (tmpIndex = 0; tmpIndex < uwBufferLenght; tmpIndex++)
    {
        pBuffer[tmpIndex] = tmpIndex + test;
    }
}

/**
* @brief  Compares two buffers.
* @param  pBuffer1, pBuffer2: buffers to be compared.
* @param  BufferLength: buffer's length
* @retval 1: pBuffer identical to pBuffer1
*         0: pBuffer differs from pBuffer1
*/
static uint8_t Buffercmp(uint8_t *pBuffer1, uint8_t *pBuffer2, uint32_t BufferLength)
{
    while (BufferLength--)
    {
        if (*pBuffer1 != *pBuffer2)
        {
            return 1;
        }

        pBuffer1++;
        pBuffer2++;
    }

    return 0;
}
