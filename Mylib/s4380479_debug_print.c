/*
 * debug_print.c
 *
 *  Created on: 3/16/2019
 */

#include "s4380479_debug_print.h"
#include "main.h"

/**
 * Redirects stdout to uart
 */
void debug_putc(char c)
{
    BRD_debug_putc(c);
    // BLE_debug_putc(c);

}

/**
 * flushed stdout
 */
void debug_flush()
{
    fflush(stdout);
}

/**
 * Redirects stdin to uart
 */
unsigned char debug_getc(void)
{
    uint8_t c = '\0';

    c = BRD_debug_getc();

    return c;
}

/**
 * flushed stdin
 */
void debug_rxflush()
{
}

/**
 * handles the task safe printf command
 */
void debug_printf(const char *fmt, ...)
{

    va_list args;

    va_start(args, fmt);

    vprintf(fmt, args);

    va_end(args);
}
