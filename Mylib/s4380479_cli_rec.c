/**
 ******************************************************************************
6* @file    myoslib/s4380479_cli_rec.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   cli rec
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_cli_rec.h"
#include "s4380479_hal_imu.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* External function prototypes -----------------------------------------------*/
void **argtableRec; //variable to store file cli cmds

/**
 * This method allcoates the memory for the argtable to store file cmds.
 */
void allocate_rec_argtable()
{
    argtableRec = malloc(sizeof(struct arg_str *) * 2 + sizeof(struct arg_lit *) + sizeof(struct arg_end *) + sizeof(void *) + sizeof(char) * 10);
}

/**
 * Initialise the file cmds
 */
void s4380479_init_rec_cmd()
{
    /** file CLI CMD */
    // struct arg_rex *logCmd = arg_rex1(NULL, NULL, "file", NULL, REG_ICASE, NULL);
    struct arg_str *enableRec = arg_strn("e", "enable", "{sensor, filename}", 2, 3,
                                         "realtime logging of sensors");
    struct arg_str *disableRec = arg_strn("d", "disable", "{sensor, filename}", 2, 2,
                                          "disable logging");
    struct arg_lit *helpRec = arg_lit0(NULL, "help", "print this help and exit");
    struct arg_end *endRec = arg_end(20);

    //redirect error func
    enableRec->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    disableRec->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    helpRec->hdr.errorfn = (arg_errorfn *)cliErrorFunc;
    endRec->hdr.errorfn = (arg_errorfn *)cliErrorFunc;

    //alocate and populate argtable with cmds
    allocate_rec_argtable();
    argtableRec[recEnable] = enableRec;
    argtableRec[recDisable] = disableRec;
    argtableRec[recHelp] = helpRec;
    argtableRec[recEnd] = endRec;
}

/**
 * This method return the artable for the file cmd
 */
void **s4380479_get_rec_cmd()
{
    return argtableRec;
}

/**
 * This is the callback function to hadndle file cmds
 */
void handle_rec(int recEnb, char *sensor, char *file, char* rate,
                int recDsb, char *sensorName, char *fileName)
{
    if (recEnb)
    {
        s4380479_os_log_message(LOG_MSG, "Sensor:%s|File:%s|%d", sensor, file, atoi(rate));
        
        if (strcmp(sensor, "acc") == 0) { //create acc file
            s4380479_os_imu_log(OPEN_ACC_FILE, file);
            toggle_sensor_status(ACC_SENSOR, SENSOR_ENABLE);
            if (atoi(rate) > 0) {
                s4380479_change_sample_rate(ACC_SENSOR, atoi(rate));
            }
            

        } else if (strcmp(sensor, "mag") == 0) { //create mag file
            s4380479_os_imu_log(OPEN_MAG_FILE, file);
            toggle_sensor_status(MAG_SENSOR, SENSOR_ENABLE);
            if (atoi(rate) > 0) {
                s4380479_change_sample_rate(MAG_SENSOR, atoi(rate));
            }

        } else {
            s4380479_os_log_message(LOG_MSG, "Invalid Sensor");
        }
    }
    else if (recDsb)
    {
        if (strcmp(sensorName, "acc") == 0) { //create acc file
            toggle_sensor_status(ACC_SENSOR, SENSOR_DISABLE);
            s4380479_os_imu_log(CLOSE_ACC_FILE," ");

        } else if (strcmp(sensorName, "mag") == 0) { //create mag file
            toggle_sensor_status(MAG_SENSOR, SENSOR_DISABLE);
            s4380479_os_imu_log(CLOSE_MAG_FILE," ");  
        } else {
            s4380479_os_log_message(LOG_MSG, "Invalid Sensor");
        }
    }
}
