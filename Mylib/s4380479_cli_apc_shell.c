/**
 ******************************************************************************
 * @file    mylib/s4380479_cli_apc_shell.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   apc shell commands drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * s4380479_cli_apc_shell_init(void); - init apc shell commands
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f4xx_hal_conf.h"
#include "nrf24l01plus.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "FreeRTOS_CLI.h"
#include "s4380479_cli_apc_shell.h"
#include "string.h"
#include "s4380479_os_radio.h"

int taskCount = 0; // count how many tasks has been displayed
UBaseType_t uxSizeOfArray; // number of tasks running
TaskStatus_t *xTaskData;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

CLI_Command_Definition_t xGetSys = { /* Structure that defines the "getsys" command line command. */
"getsys", "getsys: Prints the current system time\r\n", prvGetSysCommand, 0 };

CLI_Command_Definition_t xSetChan = { /* Structure that defines the "setchan" command line command. */
"setchan", "setchan: <chan> \r\n", prvSetChanCommand, 1 };

CLI_Command_Definition_t xGetChan = { /* Structure that defines the "getchan" command line command. */
"getchan", "getchan: Prints the radio channel\r\n", prvGetChanCommand, 0 };

CLI_Command_Definition_t xSetTxAddr = { /* Structure that defines the "settxaddr" command line command. */
"settxaddr", "settxaddr: <TX address> \r\n", prvSetTxAddrCommand, 1 };

CLI_Command_Definition_t xSetRxAddr = { /* Structure that defines the "setrxaddr" command line command. */
"setrxaddr", "setrxaddr: <RX address>\r\n", prvSetRxAddrCommand, 1 };

CLI_Command_Definition_t xGetTxAddr = { /* Structure that defines the "gettxaddr" command line command. */
		"gettxaddr", "gettxaddr: Prints the radio TX address\r\n",
		prvGetTxAddrCommand, 0 };

CLI_Command_Definition_t xGetRxAddr = {
/* Structure that defines the "getrxaddr" command line command. */
"getrxaddr", "getrxaddr: Prints the radio RX address\r\n", prvGetRxAddrCommand,
		0 };

CLI_Command_Definition_t xJoin = {
/* Structure that defines the "join" command line command. */
"join", "join: Sends a join packet to plotter\r\n", prvJoinCommand, 0 };

CLI_Command_Definition_t xMove =
		{
		/* Structure that defines the "move" command line command. */
		"move",
				"move: <X Coordinate> <Y Coordinate> Moves the plotter head to X and Y\r\n",
				prvMoveCommand, 2};

CLI_Command_Definition_t xPen = { /* Structure that defines the "move" command line command. */
"pen", "pen: <up or down> Moves the plotter head up or down\r\n", prvPenCommand,
		1 };

CLI_Command_Definition_t xStatus = { /* Gives details of all running tasks */
"status", "status: Shows each tasks memory and usage\r\n", prvStatusCommand, 0 };

CLI_Command_Definition_t xDelete = { /* Structure that defines the "move" command line command. */
"delete", "delete: <Task to delete> \r\n", prvDeleteCommand, 1 };

CLI_Command_Definition_t xCreate = { /* Structure that defines the "move" command line command. */
"create", "<name of new task>\r\n", prvCreateCommand, 1 };

void sample_task(void* param) {

	for (;;) {

		//keep looping forever
	}
}

/**
 * @brief  creates a new task
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvCreateCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;

	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	xTaskCreate(sample_task, cCmd_string, configMINIMAL_STACK_SIZE, NULL,
			tskIDLE_PRIORITY, NULL);
	xWriteBufferLen = sprintf((char *) pcWriteBuffer,
			"Task successfully created\n\r");

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Deletes a task
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvDeleteCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;

	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	TaskHandle_t currentTask = xTaskGetHandle(cCmd_string);

	if (currentTask != NULL) {

		vTaskDelete(currentTask);
		/* Write command echo output string to write buffer. */
		xWriteBufferLen = sprintf((char *) pcWriteBuffer,
				"Delete succesful\n\r");
	} else {

		xWriteBufferLen = sprintf((char *) pcWriteBuffer,
				"Invalid task or handler\n\r");
	}

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  System status command
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvStatusCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	unsigned long systemRunTime;
	char currentStatus[100]; // max buffer for status

	if (taskCount == 0) { //first time in this command

		//get the number of tasks currently running
		uxSizeOfArray = uxTaskGetNumberOfTasks();
		//array to store each task details
		xTaskData = pvPortMalloc(uxSizeOfArray * (sizeof(TaskStatus_t)));
		uxTaskGetSystemState(xTaskData, uxSizeOfArray, &systemRunTime);
		/* Write command echo output string to write buffer. */
		xWriteBufferLen = sprintf((char *) pcWriteBuffer,
				"Name, Number, Status, Priority, Stack, Runtime\r\n\r\n");

		taskCount++;

		return pdTRUE;
	}

	/*extract details of each task */
	const char* name = xTaskData[taskCount - 1].pcTaskName;
	UBaseType_t num = xTaskData[taskCount - 1].xTaskNumber;
	eTaskState status = xTaskData[taskCount - 1].eCurrentState;
	UBaseType_t priority = xTaskData[taskCount - 1].uxCurrentPriority;
	uint32_t runTime = xTaskData[taskCount - 1].ulRunTimeCounter;
	uint16_t stack = xTaskData[taskCount - 1].usStackHighWaterMark;

	/* convert status to humand radble format */
	switch (status) {

		case eBlocked:

			sprintf(currentStatus, "Block");
			break;
		case eSuspended:

			sprintf(currentStatus, "Block");
			break;

		case eRunning:

			sprintf(currentStatus, "Running");
			break;

		case eReady:

			sprintf(currentStatus, "Ready");
			break;
		default:

			sprintf(currentStatus, "Invalid status");
			break;
	}

	xWriteBufferLen = sprintf((char *) pcWriteBuffer,
			"%s, %d, %s, %d, %lu, %d\n\r", name, num, currentStatus, priority,
			runTime, stack);

	if (taskCount++ < uxSizeOfArray - 1) {

		return pdTRUE;
	} else {

		taskCount = 0; //reset count
		vPortFree(xTaskData);
	}
	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Get system time command
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvGetSysCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	uint32_t sysRunTime = HAL_GetTick();

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer,
			"Current System Time: %d (ms)\n\r", (int) sysRunTime);

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Set channel command.
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvSetChanCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;
	struct RadioMsg msg;

	msg.mode = SET_CHAN_MODE;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s\n\r",
			cCmd_string);
	if (atoi(cCmd_string)) {

		msg.chan = atoi(cCmd_string);
		if ( xQueueSend(s4380479_QueueRadio, (void *) &msg,
				( portTickType ) 10) != pdPASS) {
			//failed
		}
	} else {
		myprintf("%s", "Invalid parameter\r\n");
	}

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Get Radio channel
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvGetChanCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\r\n");

	xSemaphoreGive(s4380479_SemaphoreGetChan);

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Set radio TX address.
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvSetTxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;
	struct RadioMsg msg;

	msg.mode = SET_TX_MODE;
	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\r\n");

	strcpy(msg.addr, cCmd_string);
	if ( xQueueSend(s4380479_QueueRadio, (void *) &msg,
			( portTickType ) 10) != pdPASS) {
		//failed
	}
	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Set readio RX address
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvSetRxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;
	struct RadioMsg msg;

	msg.mode = SET_RX_MODE;
	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s\n\r",
			cCmd_string);

	strcpy(msg.addr, cCmd_string);
	if ( xQueueSend(s4380479_QueueRadio, (void *) &msg,
			( portTickType ) 10) != pdPASS) {
		//failed
	}

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Get Radio TX address
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvGetTxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\r\n", cCmd_string);
	xSemaphoreGive(s4380479_SemaphoreGetTx);

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}
/**
 * @brief  Get Radio RX address
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvGetRxAddrCommand(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\r\n", cCmd_string);
	xSemaphoreGive(s4380479_SemaphoreGetRx);

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Join packet to plotter
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvJoinCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\r%s\n", cCmd_string);
	xSemaphoreGive(s4380479_SemaphoreJoin);

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Join packet to plotter
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvMoveCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long xParam_len, yParam_len;
	char *xInput, *yInput;
	struct RadioMove msg;

	/* Get parameters from command string */
	xInput = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParam_len);
	yInput = FreeRTOS_CLIGetParameter(pcCommandString, 2, &yParam_len);

	xInput[xParam_len] = 0x00;
	yInput[yParam_len] = 0x00;

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s %s\n\r", xInput,
			yInput);

	if (s4380479_radio_is_number(xInput) && s4380479_radio_is_number(yInput)) {

		// convert x and y input to int
		msg.x = atoi(xInput);
		msg.y = atoi(yInput);

		if ( xQueueSend(s4380479_QueueMove, (void *) &msg,
				( portTickType ) 10) != pdPASS) {
			//failed
		}
	}

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * @brief  Join packet to plotter
 * @param  writebuffer, writebuffer length and command strength
 * @retval None
 */
static BaseType_t prvPenCommand(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {

	long lParam_len;
	const char *cCmd_string;

	/* Get parameters from command string */
	cCmd_string = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParam_len);

	/* Write command echo output string to write buffer. */
	xWriteBufferLen = sprintf((char *) pcWriteBuffer, "\n\r%s\n\r",
			cCmd_string);

	if (!strcmp(cCmd_string, "up")) {

		//pen up
		xSemaphoreGive(s4380479_SemaphorePenUp);
	} else if (!strcmp(cCmd_string, "down")) {

		//pen down
		xSemaphoreGive(s4380479_SemaphorePenDown);
	} else {

		myprintf("%s\r\n", "Invalid Command");
	}

	/* Return pdFALSE, as there are no more strings to return */
	/* Only return pdTRUE, if more strings need to be printed */
	return pdFALSE;
}

/**
 * Initialises the APC shell commands
 */
extern void s4380479_cli_apc_shell_init(void) {

	FreeRTOS_CLIRegisterCommand(&xGetSys);
	FreeRTOS_CLIRegisterCommand(&xSetChan);
	FreeRTOS_CLIRegisterCommand(&xGetChan);
	FreeRTOS_CLIRegisterCommand(&xSetTxAddr);
	FreeRTOS_CLIRegisterCommand(&xSetRxAddr);
	FreeRTOS_CLIRegisterCommand(&xGetTxAddr);
	FreeRTOS_CLIRegisterCommand(&xGetRxAddr);
	FreeRTOS_CLIRegisterCommand(&xJoin);
	FreeRTOS_CLIRegisterCommand(&xMove);
	FreeRTOS_CLIRegisterCommand(&xPen);
	FreeRTOS_CLIRegisterCommand(&xStatus);
	FreeRTOS_CLIRegisterCommand(&xDelete);
	FreeRTOS_CLIRegisterCommand(&xCreate);
}

