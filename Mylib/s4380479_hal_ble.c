/**
 ******************************************************************************
6* @file    myoslib/s4380479_hal_ble.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   hal ble drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_hal_ble.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t SERVER_BDADDR[] = {0x00, 0x11, 0x91, 0x47, 0x80, 0x43};
uint8_t bdaddr[BDADDR_SIZE];
extern volatile uint8_t set_connectable;

/* External function prototypes -----------------------------------------------*/

void s4380479_init_BLE(void)
{

    int ret;

    // hci init functions
    hci_init(s4380479_scan_user_notify, NULL);
    hci_reset();
    HAL_Delay(100);

    //init gatt
    BLUENRG_memcpy(bdaddr, SERVER_BDADDR, sizeof(SERVER_BDADDR));
    ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET,
                                    CONFIG_DATA_PUBADDR_LEN,
                                    bdaddr);
    if (ret)
    {
        printf("Setting BD_ADDR failed 0x%02x.\r\n", ret);
    }
    printf("0x%02x:%02x:%02x:%02x:%02x:%02x\r\n", 
        SERVER_BDADDR[5], SERVER_BDADDR[4], SERVER_BDADDR[3], 
        SERVER_BDADDR[2], SERVER_BDADDR[1], SERVER_BDADDR[0]);

    ret = aci_gatt_init();
    if (ret)
    {
        printf("GATT_Init failed.\r\n");
    }
    printf("BLE INITIALISED.\r\n");
}
