/**
 ******************************************************************************
 * @file    mylib/s4380479_os_radio.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2018
 * @brief   pantilt os drivers
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 * extern void s4380479_TaskRadio(void) - Task to control the aradio messagest,
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal_conf.h"
#include "debug_printf.h"
#include "s4380479_hal_sysmon.h"
#include "FreeRTOS.h"
#include "s4380479_os_radio.h"
#include "s4380479_os_printf.h"
#include "s4380479_cli_apc_graphics.h"
#include <math.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
#define RADIO_TASK_STACK_SIZE		(configMINIMAL_STACK_SIZE * 2)
#define RADIO_PRIORITY 				(tskIDLE_PRIORITY + 5)

/* task handlers */
TaskHandle_t s4380479_RADIO_Handle;
TaskHandle_t s4380479_RADIO_FSM_Handle;

uint8_t baseTxAddr[] = { 0x46, 0x33, 0x22, 0x11, 0x00 };
uint8_t baseRxAddr[] = { 0x45, 0x33, 0x22, 0x11, 0x00 };

unsigned char rxPacket[32];
unsigned char packetBuffer[32] = { 0xA1, 0x46, 0x33, 0x22, 0x11, 0x91, 0x47,
		0x80, 0x43, 0x00 };
unsigned char ackMsg[22] = { 0x47, 0x1E, 0x2B, 0x0, 0x47, 0x35, 0x2B, 0x0, 0x47,
		0xB8 };
unsigned char errMsg[22] = { 0x47, 0x59, 0x2B, 0x0, 0x59, 0x2b, 0x2B, 0x0, 0x59,
		0x2B };

int ackReceived = 0;

/**
 * Radio fsm processing task
 */
void s4380479_Task_Fsm(void *param) {

	while (1) {

		s4380479_hal_radio_fsmprocessing();

		if (s4380479_hal_radio_getrxstatus() == 1) {

			/*  process received message */
			int i;
			for (i = 0; i < 32; i++) {
				rxPacket[i] = 0;
			}
			s4380479_hal_radio_getpacket(rxPacket);
			myprintf("%s", "Received from radio : ");
			for (i = 10; i < 32; i += 2) {
				myprintf("%c",
						s4380479_hal_hamming_decoder((int ) rxPacket[i]) << 4
								| s4380479_hal_hamming_decoder(
										(int ) rxPacket[i + 1]));
			}
			myprintf("%s", " \r\n");
			xSemaphoreGive(s4380479_SemaphoreReceive);
		}
		vTaskDelay(100);
	}
}

/**
 * Sends the x and y coordinates to set the pan/tilt module
 */
void set_pantilt_angle(int x, int y) {

	struct PanMessage msgPan;
	struct TiltMessage msgTilt;

	msgPan.angle = x;
	if (xQueueSend(s4387479_QueuePan, (void *) &msgPan,
			( portTickType ) 10) != pdPASS) {
		//failed
	}
	vTaskDelay(25);

	msgTilt.angle = y;
	if (xQueueSend(s4387479_QueueTilt, (void *) &msgTilt,
			( portTickType ) 10) != pdPASS) {
		//failed
	}
}

/**
 * Check if the received message is ACK/ERR
 *
 * return int to suggest the message received
 */
int check_message(void) {

	int j, match = 1;
//if msg ack
	for (j = 0; j < MAX_BYTES; j++) {
		if (ackMsg[j] != rxPacket[j + 10]) {

			match = 0;
			break;
		} else {

			match = ACK_MESSAGE;
		}
	}
	if (!match) {

		//if msg err
		for (j = 0; j < MAX_BYTES; j++) {
			if (errMsg[j] != rxPacket[j + 10]) {

				match = 0;
				break;
			} else {

				match = ERR_MESSAGE;
			}
		}
	}

	return match;
}

/**
 * Draw square side
 *
 * (This function assumes the pen is already in the correct position)
 */
int move_position(int x, int y) {

	struct DrawLine msg;

	/* move to the curent poistion */
	msg.x = x;
	msg.y = y;
	//reposition
	if (xQueueSend(s4380479_QueueMove, (void *) &msg,
			( portTickType ) 10) != pdPASS) {
		//failed
	}
	if (s4380479_SemaphoreAck != NULL &&
	xSemaphoreTake(s4380479_SemaphoreAck,
			portTICK_RATE_MS * ACK_TIMEOUT * 3) == pdTRUE) {

		vTaskDelay(100);
		return 1;
	}

	vTaskDelay(100);
	return 0;
}

/**
 * Draws a straight line of given length from the given x and y positions.
 */
int draw_line(int x, int y, char type, int length) {

	/* send pen up command */
	xSemaphoreGive(s4380479_SemaphorePenUp);

	if (s4380479_SemaphoreAck != NULL &&
	xSemaphoreTake(s4380479_SemaphoreAck,
			portTICK_RATE_MS * ACK_TIMEOUT * 3) == pdTRUE) {

		/* move to the curent poistion */
		if (move_position(x, y)) {

			/* send pen dowm command */
			xSemaphoreGive(s4380479_SemaphorePenDown);
			if (s4380479_SemaphoreAck != NULL &&
			xSemaphoreTake(s4380479_SemaphoreAck,
					portTICK_RATE_MS * ACK_TIMEOUT * 3) == pdTRUE) {

				/* move to next position to draw line */
				if (type == 'h') {

					move_position(x + length, y);
				} else {

					move_position(x, y + length);
				}
				return 1;
			}

		}
	}

	return 0;
}

/**
 * Draws bline with paramaters given
 *
 * Pseudocode reference: http://www.uobabylon.edu.iq/eprints/publication_2_22893_6215.pdf
 *
 */
void draw_bline(int x, int y, int x2, int y2, int step) {

	int w = x2 - x;
	int h = y2 - y;
	int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;

	/* using gradion to find whihc octant the line needs to be drawn */
	if (w < 0) {

		dx1 = -step;
	} else if (w > 0) {

		dx1 = step;
	}
	if (h < 0) {

		dy1 = -step;
	} else if (h > 0) {

		dy1 = step;
	}
	if (w < 0) {

		dx2 = -step;
	} else if (w > 0) {

		dx2 = step;
	}
	/* calculate longest and shortest pat */
	int longest = abs(w) / step;
	int shortest = abs(h) / step;

	if (!(longest > shortest)) {

		longest = abs(h) / step;
		shortest = abs(w) / step;

		if (h < 0) {

			dy2 = -step;
		} else if (h > 0) {

			dy2 = step;
		}
		dx2 = 0;
	}
	int numerator = longest >> 1;

	/* send pen up command */
	xSemaphoreGive(s4380479_SemaphorePenUp);
	if (s4380479_SemaphoreAck != NULL &&
	xSemaphoreTake(s4380479_SemaphoreAck,
			portTICK_RATE_MS * ACK_TIMEOUT * 3) == pdTRUE) {

		/* move to the curent poistion */
		if (move_position(x, y)) {

			/* send pen dowm command */
			xSemaphoreGive(s4380479_SemaphorePenDown);
			for (int i = 0; i <= longest; i++) {

				move_position(x, y);
				numerator += shortest;
				if (!(numerator < longest)) {
					numerator -= longest;
					x += dx1;
					y += dy1;
				} else {
					x += dx2;
					y += dy2;
				}
			}
		}
	}
}

/**
 * Generate the radio packet to send
 */
void build_send_packet(char* payload) {

	int i = 1, j = 0; //add tx addr
	uint8_t addr[5] = { 0 };

	int x;
	//clear packet
	for (x = 10; x < 32; x++) {

		packetBuffer[x] = 0;
	}

	/* add addresses to packets */
	s4380479_hal_radio_idle();
	s4380479_hal_radio_gettxaddres(addr);
	for (; i < 5; i++) {

		packetBuffer[i] = addr[i - 1];
	}

	s4380479_hal_radio_idle();
	s4380479_hal_radio_getrxaddres(addr);
	for (; i < 9; i++) {

		packetBuffer[i] = addr[i - 5];
	}
	i++;
	// add encoded payload to packet
	for (; j < strlen(payload); j++) {

		packetBuffer[i++] = s4380479_hal_hamming_byte_encoder(payload[j]) >> 8;
		packetBuffer[i++] = s4380479_hal_hamming_byte_encoder(payload[j])
				& 0x00FF;
	}
}

/**
 * Sends the radio apcket and updates fsm
 */
int send_packet_radio(void) {

	int count = 3, err = 0; //wait 3 times before give up

	while (count || err) {

		err = 0;
		//send packet
		s4380479_hal_radio_idle();
		s4380479_hal_radio_sendpacket(s4380479_hal_radio_getchan(), baseTxAddr,
				packetBuffer);
		s4380479_hal_radio_setfsmrx();

		if (s4380479_SemaphoreReceive != NULL &&
		xSemaphoreTake(s4380479_SemaphoreReceive,
				portTICK_RATE_MS * ACK_TIMEOUT) == pdTRUE) {

			if (check_message() == ACK_MESSAGE) {

				return ACK_MESSAGE;
			} else if (check_message() == ERR_MESSAGE) {

				err = 1;
			}
		}
		count--;
	}

	return GAVE_UP;
}

/**
 * Initialises the pins used for the system monitor
 */
void s4380479_TaskRadio(void* param) {

	struct RadioMsg msg;
	struct RadioMove moveMsg;

	const char* pos;
	char move[MAX_ADDR_LENGTH]; //move string
	uint8_t addr[5] = { 0 }; // variable to store tx/rx addr
	int penStatus = Z_PEN_DOWN; // penup / pendown
	int xPos = 0, yPos = 0; //assume the x and y at origin

	while (1) {

		/* Getting channel and RX/TX address */
		if (s4380479_SemaphoreGetChan != NULL &&
		xSemaphoreTake(s4380479_SemaphoreGetChan, 1) == pdTRUE) {

			//print radio channel
			s4380479_hal_radio_idle();
			myprintf("Radio Channel: %d\r\n", s4380479_hal_radio_getchan());

		} else if (s4380479_SemaphoreGetTx != NULL &&
		xSemaphoreTake(s4380479_SemaphoreGetTx, 1) == pdTRUE) {

			s4380479_hal_radio_idle();
			s4380479_hal_radio_gettxaddres(addr);
			myprintf("Radio TX address: %02X%02X%02X%02X%02X\r\n", addr[0],
					addr[1], addr[2], addr[3], addr[4]);

		} else if (s4380479_SemaphoreGetRx != NULL &&
		xSemaphoreTake(s4380479_SemaphoreGetRx, 1) == pdTRUE) {

			//print rx address
			s4380479_hal_radio_idle();
			s4380479_hal_radio_getrxaddres(addr);
			myprintf("Radio RX address: %02X%02X%02X%02X%02X\r\n", addr[0],
					addr[1], addr[2], addr[3], addr[4]);

		} else if (s4380479_SemaphorePenUp != NULL &&
		xSemaphoreTake(s4380479_SemaphorePenUp, 1) == pdTRUE) {

			sprintf(move, "XYZ%03d%03d%02d", xPos, yPos, Z_PEN_UP);
			build_send_packet(move);
			if (send_packet_radio() == ACK_MESSAGE) {

				penStatus = Z_PEN_UP;
				set_pantilt_angle(xPos, yPos);
				xSemaphoreGive(s4380479_SemaphoreAck);

			}
		} else if (s4380479_SemaphorePenDown != NULL &&
		xSemaphoreTake(s4380479_SemaphorePenDown, 1) == pdTRUE) {

			sprintf(move, "XYZ%03d%03d%02d", xPos, yPos, Z_PEN_DOWN);
			build_send_packet(move);
			if (send_packet_radio() == ACK_MESSAGE) {

				penStatus = Z_PEN_DOWN;
				set_pantilt_angle(xPos, yPos);
				xSemaphoreGive(s4380479_SemaphoreAck);
			}
		} else if (s4380479_SemaphoreJoin != NULL &&
		xSemaphoreTake(s4380479_SemaphoreJoin, 1) == pdTRUE) {

			build_send_packet("JOIN");
			if (send_packet_radio() == ACK_MESSAGE) {

				// join succesful
			}
		} else if (s4380479_SemaphoreOrigin != NULL &&
		xSemaphoreTake(s4380479_SemaphoreOrigin, 1) == pdTRUE) {

			sprintf(move, "XYZ%03d%03d%02d", 0, 0, Z_PEN_DOWN);
			build_send_packet(move);
			if (send_packet_radio() == ACK_MESSAGE) {

				xPos = 0, yPos = 0;
				penStatus = Z_PEN_DOWN;
				set_pantilt_angle(xPos, yPos);
				xSemaphoreGive(s4380479_SemaphoreAck);
			}
		}

		/* Setting channel and RX/TX address */
		if (s4380479_QueueRadio != NULL) {

			if (xQueueReceive(s4380479_QueueRadio, &msg, 10)) {

				s4380479_hal_radio_idle();
				//queue has a message
				switch (msg.mode) {
					case SET_CHAN_MODE:

						s4380479_hal_radio_setchan(msg.chan);
						break;

					case SET_TX_MODE:

						pos = msg.addr;
						pos += 6;
						for (int i = 0; i < 4; ++i) {

							sscanf(pos, "%2hhx", &addr[i]);
							pos -= 2;
						}

						s4380479_hal_radio_settxaddres(addr);
						break;

					case SET_RX_MODE:

						pos = msg.addr;
						pos += 6;
						for (int i = 0; i < 4; ++i) {

							sscanf(pos, "%2hhx", &addr[i]);
							pos -= 2;
						}
						s4380479_hal_radio_setrxaddres(addr);
						break;

					default:

						break;
				}

			}
		}

		if (s4380479_QueueMove != NULL) { //move command queue

			if (xQueueReceive(s4380479_QueueMove, &moveMsg, 1)) {

				sprintf(move, "XYZ%03d%03d%02d", moveMsg.x, moveMsg.y,
						penStatus);
				build_send_packet(move);
				if (send_packet_radio()) {

					//use x and y values
					xPos = moveMsg.x;
					yPos = moveMsg.y;
					set_pantilt_angle(xPos, yPos);
					xSemaphoreGive(s4380479_SemaphoreAck);
				}
			}
		}

		vTaskDelay(100);
	}

}

/**
 *	radio task init function
 */
extern void s4380479_os_radio_init() {

	/* init radio and set base addresses */
	s4380479_hal_radio_init();
	s4380479_hal_radio_settxaddres(baseTxAddr);
	s4380479_hal_radio_setrxaddres(baseRxAddr);

	/* create queues and semaphores*/
	s4380479_SemaphoreGetChan = xSemaphoreCreateBinary();
	s4380479_SemaphoreGetRx = xSemaphoreCreateBinary();
	s4380479_SemaphoreGetTx = xSemaphoreCreateBinary();
	s4380479_SemaphoreJoin = xSemaphoreCreateBinary();
	s4380479_SemaphorePenUp = xSemaphoreCreateBinary();
	s4380479_SemaphorePenDown = xSemaphoreCreateBinary();
	s4380479_SemaphoreOrigin = xSemaphoreCreateBinary();
	s4380479_SemaphoreReceive = xSemaphoreCreateBinary();
	s4380479_SemaphoreAck = xSemaphoreCreateBinary();

	/*init quueus */
	s4380479_QueueRadio = xQueueCreate(10, sizeof(struct RadioMsg));
	s4380479_QueueMove = xQueueCreate(10, sizeof(struct RadioMove));
	s4380479_QueueLine = xQueueCreate(10, sizeof(struct DrawLine));

	/* initiate task */
	xTaskCreate(&s4380479_TaskRadio, "Radio", RADIO_TASK_STACK_SIZE,
	NULL, RADIO_PRIORITY, s4380479_RADIO_Handle);
	xTaskCreate(&s4380479_Task_Fsm, "FSM", RADIO_TASK_STACK_SIZE, NULL,
	RADIO_PRIORITY + 1, s4380479_RADIO_FSM_Handle);
}

