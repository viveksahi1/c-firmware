/**
 ******************************************************************************
6* @file    myoslib/s4380479_ble_adv.c
 * @author  Vivek Sahi - 43804791
 * @date    08-03-2019
 * @brief   ble adv
 *
 ******************************************************************************
 *     EXTERNAL FUNCTIONS
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "s4380479_ble_adv.h"

/* Private variables ---------------------------------------------------------*/
uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
uint8_t manuf_data[] = {DATA_LENGTH, AD_TYPE_SERVICE_DATA, 0x00, 0x41, 0x41, 0x41, 0x41};

extern volatile uint8_t set_connectable;
extern volatile int connected;
extern AxesRaw_t axes_data;

const char *name = "4380479";
int adv_status = 1;
int16_t pXYZ[3] = {0};

/*
 * BlueNRG-MS init advertising
 */
void s4380479_ADV_Init(void)
{
    int ret;

    //disable ble scanning
    ret = aci_gap_terminate_gap_procedure(GAP_GENERAL_DISCOVERY_PROC);
    if (ret != BLE_STATUS_SUCCESS)
    {
        s4380479_os_log_message(ERROR_MSG, "terminate GAP_Init failed. %d \r\n", ret);
    }
    s4380479_os_log_message(ERROR_MSG, "terminated scan\r\n");

    //setup server
    ret = aci_gap_init_IDB05A1(GAP_PERIPHERAL_ROLE_IDB05A1, 0, 0x07, &service_handle, &dev_name_char_handle, &appearance_char_handle);

    if (ret != BLE_STATUS_SUCCESS)
    {
        s4380479_os_log_message(ERROR_MSG, "GAP_Init failed. %d \r\n", ret);
    }

    ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle, 0,
                                     strlen(name), (uint8_t *)name);

    if (ret)
    {
        s4380479_os_log_message(ERROR_MSG, "aci_gatt_update_char_value failed.\r\n");
        while (1)
            ;
    }

    ret = aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED,
                                       OOB_AUTH_DATA_ABSENT,
                                       NULL,
                                       7,
                                       16,
                                       USE_FIXED_PIN_FOR_PAIRING,
                                       123456,
                                       BONDING);
    if (ret != BLE_STATUS_SUCCESS)
    {
        s4380479_os_log_message(ERROR_MSG, "BLE Stack not Initialized\r\n");
    }

    //add service
    ret = Add_Acc_Service();
    if (ret != BLE_STATUS_SUCCESS)
    {
        s4380479_os_log_message(ERROR_MSG, "Error while adding Acc service.\r\n");
    }

    /* Set output power level */
    ret = aci_hal_set_tx_power_level(1, 4);
    s4380479_start_advertising(); //start advertising
    // set_connectable = TRUE;
}

/*
 * BlueNRG-MS toggle advertising
 */
void s4380479_toggle_advertising(int status)
{

    hci_le_set_advertise_enable(status);
    // if (!status)
    // {
    //     // int ret =
    //     hci_le_set_advertise_enable(0);
    //     adv_status = 0;
    // }
    // else
    // {
    //     // set_connectable = TRUE;
    //     adv_status = 1;
    // }
}

/*
 * BlueNRG-MS get adv status
 */
int s4380479_get_adv_status(void)
{
    return adv_status;
}

/**
 * @brief  Start the advertising
 *
 * @param  None
 * @retval None
 */
void s4380479_start_advertising(void)
{

    s4380479_os_log_message(LOG_MSG, "ADVERTISING STARTING\r\n");
    setConnectable();
}

/**
 * @brief  Update the advertising packet
 *
 * @param  None
 * @retval None
 */
void s4380479_update_advertising(void)
{
    uint32_t distance = s4380479_get_echo_time();
    tBleStatus ret = aci_gap_update_adv_data(DATA_LENGTH + 2, manuf_data);
    manuf_data[2] =  distance >> 24;
    manuf_data[3] =  distance >> 16;
    manuf_data[4] =  distance >> 8;
    manuf_data[5] =  distance;
    printf("Updated Value %02X %02X %02X %02X\r\n", manuf_data[2],manuf_data[3],manuf_data[4],manuf_data[5]);
    hci_user_evt_proc();
}

/**
 * @brief  Puts the device in connectable mode.
 *         If you want to specify a UUID list in the advertising data, those data can
 *         be specified as a parameter in aci_gap_set_discoverable().
 *         For manufacture data, aci_gap_update_adv_data must be called.
 * @param  None 
 * @retval None
 */
void s4380479_setConnectable(void)
{
    tBleStatus ret;

    const char local_name[] = {AD_TYPE_COMPLETE_LOCAL_NAME, '4', '3', '8', '0', '4', '7', '9', '1'};
    /* disable scan response */
    hci_le_set_scan_resp_data(0, NULL);
    printf("General Discoverable Mode.\r\n");

    ret = aci_gap_set_discoverable(ADV_DATA_TYPE, ADV_INTERV_MIN, ADV_INTERV_MAX, PUBLIC_ADDR,
                                   NO_WHITE_LIST_USE, sizeof(local_name), local_name, 0, NULL, 0, 0);
    if (ret != BLE_STATUS_SUCCESS)
    {
        printf("Error while setting discoverable mode (%d)\r\n", ret);
    }
}

/**
 * @brief  Configure the device as Client/server to communicate.
 *
 * @param  None
 * @retval None
 */
// void s4380479_ADV_user_process(void)
// {

//     AxesRaw_t *p_axes = &axes_data;
//     s4380479_os_log_message(DEBUG_MSG, "running");
//     if (set_connectable)
//     {
//         /* Establish connection with remote device */
//         s4380479_os_log_message(DEBUG_MSG, "ADVERTISING STARTING\r\n");
//         setConnectable();
//         set_connectable = FALSE;
//         //   s4380479_os_log_message(DEBUG_MSG, "status %d",hci_le_set_advertise_enable(0));
//     }
//     if (0) //connected && s4380479_get_adv_status())
//     {
//         printf("SENDING ACC READINGS\r\n");
//         // BSP_ACCELERO_AccGetXYZ(pDataXYZ); //retrieve accelerometer readings

//         /* Update acceleration data */
//         p_axes->AXIS_X = pXYZ[0]++;
//         p_axes->AXIS_Y = pXYZ[1];
//         p_axes->AXIS_Z = 0;
//         Acc_Update(p_axes);
//         osDelay(1000); // send reading at 1Hz
//     }
// }

/*
 * BlueNRG-MS background scan task
 */
// void s4380479_ADV_process(void)
// {
//     s4380479_ADV_user_process();
//     hci_user_evt_proc();
// }
