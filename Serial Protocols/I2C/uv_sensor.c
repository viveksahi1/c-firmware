#include "uv_sensor.h"

/**
 * @brief i2c master initialization
 */
void i2c_example_master_init(void)
{
	int i2c_master_port = I2C_EXAMPLE_MASTER_NUM;
	i2c_config_t conf;
	conf.mode = I2C_MODE_MASTER;
	conf.sda_io_num = I2C_EXAMPLE_MASTER_SDA_IO;
	conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
	conf.scl_io_num = I2C_EXAMPLE_MASTER_SCL_IO;
	conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
	conf.master.clk_speed = I2C_EXAMPLE_MASTER_FREQ_HZ;
	i2c_param_config(i2c_master_port, &conf);
	i2c_driver_install(i2c_master_port, conf.mode,
					   I2C_EXAMPLE_MASTER_RX_BUF_DISABLE,
					   I2C_EXAMPLE_MASTER_TX_BUF_DISABLE, 0);
}

/* UV sensor read/write */
esp_err_t i2c_example_master_sensor_test(i2c_port_t i2c_num, uint8_t *data_h, uint8_t *data_l, uint8_t reg)
{
	//Write to register
	int ret;
	uint8_t data = 0;

	data |= VEML6075_SENSOR_ADDR; //0x10
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, VEML6075_SENSOR_ADDR << 1 | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, VEML6075_REG_CONF, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, (uint8_t)(0xFF & (data >> 0)), ACK_CHECK_EN);
	i2c_master_write_byte(cmd, (uint8_t)(0xFF & (data >> 8)), ACK_CHECK_EN);
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);

	/* check for errors */
	if (ret != ESP_OK)
	{
		return ret;
	}

	vTaskDelay(30 / portTICK_RATE_MS); // delay between read and write
	/* read from UV registers */
	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, VEML6075_SENSOR_ADDR << 1 | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, VEML6075_SENSOR_ADDR << 1 | READ_BIT, ACK_CHECK_EN);
	i2c_master_read_byte(cmd, data_l, ACK_VAL);
	i2c_master_read_byte(cmd, data_h, ACK_VAL);
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);

	return ret;
}

/** 
 * Retrieves the data from the sensor registers
 */
int sensor_print(void)
{
	int ret;
	uint8_t sensor_data_h, sensor_data_l;
	float UVA = 0, UVB = 0, DUMMY = 0, VC = 0, IR = 0;
	float c_vc, c_ir, c_uva, c_uvb;

	//UVA
	ret = i2c_example_master_sensor_test(I2C_EXAMPLE_MASTER_NUM, &sensor_data_h, &sensor_data_l, VEML6075_REG_UVA);
	if (ret == ESP_ERR_TIMEOUT)
	{
		printf("I2C timeout\n");
	}
	else if (ret == ESP_OK)
	{
		UVA = ((sensor_data_h << 8 | sensor_data_l) / 1.2);
	}
	else
	{
		printf("%s: No ack, sensor not connected...skip...\n", esp_err_to_name(ret));
	}

	//UVB
	ret = i2c_example_master_sensor_test(I2C_EXAMPLE_MASTER_NUM, &sensor_data_h, &sensor_data_l, VEML6075_REG_UVB);
	if (ret == ESP_ERR_TIMEOUT)
	{
		printf("I2C timeout\n");
	}
	else if (ret == ESP_OK)
	{
		UVB = ((sensor_data_h << 8 | sensor_data_l) / 1.2);
	}
	else
	{
		printf("%s: No ack, sensor not connected...skip...\n", esp_err_to_name(ret));
	}

	//DUMMY
	ret = i2c_example_master_sensor_test(I2C_EXAMPLE_MASTER_NUM, &sensor_data_h, &sensor_data_l, VEML6075_REG_DUMMY);
	if (ret == ESP_ERR_TIMEOUT)
	{
		printf("I2C timeout\n");
	}
	else if (ret == ESP_OK)
	{
		DUMMY = ((sensor_data_h << 8 | sensor_data_l) / 1.2);
	}
	else
	{
		printf("%s: No ack, sensor not connected...skip...\n", esp_err_to_name(ret));
	}

	//Visible compensation register
	ret = i2c_example_master_sensor_test(I2C_EXAMPLE_MASTER_NUM, &sensor_data_h, &sensor_data_l, VEML6075_REG_UVCOMP1);
	if (ret == ESP_ERR_TIMEOUT)
	{
		printf("I2C timeout\n");
	}
	else if (ret == ESP_OK)
	{
		VC = ((sensor_data_h << 8 | sensor_data_l) / 1.2);
	}
	else
	{
		printf("%s: No ack, sensor not connected...skip...\n", esp_err_to_name(ret));
	}

	//IR compensation register
	ret = i2c_example_master_sensor_test(I2C_EXAMPLE_MASTER_NUM, &sensor_data_h, &sensor_data_l, VEML6075_REG_UVCOMP2);
	if (ret == ESP_ERR_TIMEOUT)
	{

		printf("I2C timeout\n");
	}
	else if (ret == ESP_OK)
	{
		IR = ((sensor_data_h << 8 | sensor_data_l) / 1.2);
	}
	else
	{
		printf("%s: No ack, sensor not connected...skip...\n", esp_err_to_name(ret));
	}

	//calculate UVA and UVB values
	c_vc = VC - DUMMY;
	c_ir = IR - DUMMY;
	c_uva = UVA - DUMMY;
	c_uva -= (VEML6075_UVI_UVA_VIS_COEFF * c_vc) - (VEML6075_UVI_UVA_IR_COEFF * c_ir);
	c_uvb = UVB - DUMMY;
	c_uvb -= (VEML6075_UVI_UVB_VIS_COEFF * c_vc) - (VEML6075_UVI_UVB_IR_COEFF * c_ir);

	if (c_uva > 50 && c_uvb > 50)
	{
		return 0;
	}

	return 1;
}
