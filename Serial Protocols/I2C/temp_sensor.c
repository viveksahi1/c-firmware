#include "temp_sensor.h"

/* variables that store adc */
static esp_adc_cal_characteristics_t *adc_chars;
static esp_adc_cal_characteristics_t *adc_chars_1;

//GPIO34 if ADC1, GPIO14 if ADC2 %for humidity
static const adc_channel_t channel = ADC_CHANNEL_6;
static const adc_atten_t atten = ADC_ATTEN_DB_11;
static const adc_unit_t unit = ADC_UNIT_1;

/* set adc channels */
static const adc_channel_t channel_1 = ADC_CHANNEL_7; //for temperature
static const adc_atten_t atten_1 = ADC_ATTEN_DB_11;
static const adc_unit_t unit_1 = ADC_UNIT_1;

//raw hum and temp readings
float humValue, temValueC;

/**
 * Initialise the i2c uv sensor
 */
void init_temp_sensor(void)
{
	/* Configure ADC */
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(channel, atten);
	adc1_config_channel_atten(channel_1, atten_1);

	/* Characterize ADC */
	adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
	adc_chars_1 = calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);
	esp_adc_cal_characterize(unit_1, atten_1, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars_1);
}

/* This function gets the raw adc reading from the sensor 
   and convert it temperature and humidity 
*/
void get_temp_reading(void)
{
	uint32_t hum_reading = 0, tem_reading = 0, humVoltage = 0, temVoltage = 0;
	int raw, raw_1;

	init_temp_sensor();
	/* Multisampling */
	for (int i = 0; i < NO_OF_SAMPLES; i++)
	{
		if (unit == ADC_UNIT_1)
		{
			hum_reading += adc1_get_raw((adc1_channel_t)channel);
			tem_reading += adc1_get_raw((adc1_channel_t)channel_1);
		}
	}
	hum_reading /= NO_OF_SAMPLES;
	tem_reading /= NO_OF_SAMPLES;

	/*Convert adc_reading to voltage in mV*/
	humVoltage = esp_adc_cal_raw_to_voltage(hum_reading, adc_chars);
	temVoltage = esp_adc_cal_raw_to_voltage(tem_reading, adc_chars_1);

	// formula used from seonsor datasheet
	humValue = ((125 * humVoltage) / 3600) - 12.5;
	temValueC = ((218.75 * temVoltage) / 3600) - 66.875;
	init_tone_detection();
}

/**
  * This function gets the final temperature value
  */
float get_temp_value(void)
{
	return temValueC;
}

/**
  * This function gets the final humidity value
  */
float get_hum_value(void)
{
	return humValue;
}
